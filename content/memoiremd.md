# FAIRE AVEC_ <br>Design\_graphique\_ESAD<br>\_Valence\_2021. *html*



Depuis le début de nos formations, nous apprenons tous·tes à nous servir des mêmes outils numériques propriétaires. On nous apprend souvent que la relation aux client·es doit être monétaire, ou encore que nos idées doivent être protégées par le droit d'auteurice pour éviter la réapropriation par d'autres. Il nous est demandé d'imprimer des maquettes de nos projets sans s'inquiéter de du gaspillage de papier, de sa provenance ou de la consommation d'encre. Tout ces choix, et bien d'autres que je ne cite pas ici, nous font participer à la société de surconsommation dans laquelle nous vivons aujourd'hui, alors que nous devrions plutôt la remettre en question. Évidemment, et heureusement, beaucoup d'entre nous remettons en cause tous ces acquis au fil de nos rencontres et de nos expériences. C'est ce que je vais tenter de faire ici&nbsp;: déconstruire les croyances autour d'un graphisme surconsommateur, capitaliste et propriétaire.

<!-- Cette partie devrait être en début de ton intro : pense context globale d'abords ensuite ta position de designer -->

Chaque jour un peu plus, nous sommes confronté·es à de nombreux problèmes sociétaux&nbsp;: crise économique, urgence écologique, société hyper-consumériste, inégalités sociales, inaccessibilité des savoirs. La pratique du graphisme évolue en fonction de notre société et puisque les graphistes ont un rôle politique direct ou indirect au sein de celle-ci, il me paraît important de réfléchir, d’imaginer et d’agir en conséquence.

Pour se faire, je choisie de penser de manière alternative et radicale, d'abord pour refuser les normes imposées par les systèmes dominants, mais avant tout pour créer avec des valeurs qui me semblent importantes pour une société plus juste et responsable.

Ce projet d’écriture gravite autour des notions de postures alternatives, de pensées et créations radicales, d'autonomie, d'économie et de «&nbsp;communs&nbsp;».

Comment être un·e designer graphique responsable, qui crée·e et diffuse du contenu&nbsp;? Comment faire avec ce que nous avons, en favorisant les circuits courts, sans participer à la dégradation de notre environnement&nbsp;? Comment faire sans ce qui participe à une société de plus en plus privée et inégalitaire&nbsp;?

<!-- Il vaut mieu faire une balise blockquote, qu'on pourra designer en css par blockquote{...} -->
<blockquote>«&nbsp;D'abord, parce qu'elles \les démarches d'autoproduction d'outils] font exister d’autres formes possibles de fabrication. Ensuite, parce que, plus ou moins directement, elles opèrent une remise en question radicale des valeurs dominantes, aussi bien commerciales que productives. On peut y percevoir une critique de l'industrie capitaliste telle qu'elle s'est développée depuis XIXe siècle au service de la production de marchandises. Loins d'être insignifiante, ces démarches procèdent sans aucun doute d'une mise en cause beaucoup plus profonde qu'il n'y paraît des sociétés occidentalisées et du principe de production et de consommation sur lequel elles sont fondées. C’est la nature même du métier de designer qui est interrogée, ainsi que la place et le rôle de ce dernier dans le système productiviste contemporain<span class="footnote">*Objectiver*, ouvrage collectif dirigée par David-Olivier Lartigaud, Entretien avec Sophie Fétro, p.123, 2017</span>&nbsp;»</blockquote>

<p class="sommaire">I.
Histoire d'alternatives et  radicalité </p>

1 . Mouvements alternatifs et mutation de l'édition

  * Qu'est-ce que l'alternatif pour vous&nbsp;? <br>Récolte et recueil de définitions et d’interprétations

  * Les origines de l'alternatif&nbsp;: <br>premières tentatives de reconquête des moyens de production

  * S'émanciper des contraintes&nbsp;: <br>comment repousser les limites des systèmes de production dominants

2 . Radicalité et Design

  * Pour une radicalité transformatrice&nbsp;: <br>rupture avec l’ordre établi

  * L'engagement

  * Expérimentation pour la radicalité

3 . Autonomie et graphisme&nbsp;: un acte politique

  * Créateurices et utilisateurices autonomes&nbsp;: <br> rendre les usager·ères acteurices

  * Les communs, autonomie ou co-dépendance choisie

II. <br>
Création et diffusion alternatives

1 . Détournement, réappropriation et bricolage

  * Créations de nos propres outils

  * Détourner les canaux et supports de diffusion habituels, pourquoi&nbsp;?

  * Création de nos canaux de diffusion numérique

2 . L'éco-conception

  * De la responsabilité des créateurices

  * La *low-tech*, une solution juste

Conclusion

Lexique

Mise en page

Bibliographie, webographie

Remerciements

Colophon


## I. Histoire d'alternatives et radicalité

<blockquote>«&nbsp;Le fait, pour le designer, de posséder ses propres outils et machines de repose pas principalement d'un désir d'appropriation \…]. Il est plutôt question de proximité avec la technique, ce qui leur offre surtout une plus grande liberté d’expérimentation. \…] Cela permet aussi d'oser des paramétrages moins standart, ce qui de fait, accroît les possibilités de mise en forme de la matière et élargit la nature même des propositions. \…] Cette proximité et la familiarité qui peut en découler est l'occasion de nourrir autrement l'imaginaire du designer, de favoriser une prise de conscience des possibilités offertes par les outils et les machines et, ainsi, d'ouvrir son horizon créatif<span class="footnote">*Objectiver*, ouvrage collectif dirigée par David-Olivier Lartigaud, Entretien avec Sophie Fétro, p.123, 2017</span>&nbsp;»</blockquote>


### 1. Mouvements alternatifs et mutation de l'édition

#### Qu'est-ce que l'alternatif pour vous&nbsp;? <br>Récolte et recueil de définitions et d’interprétations


Il me semble que les interprétations du mot «&nbsp;alternatif&nbsp;» sont si nombreuses qu’il en devient presque énigmatique et mystérieux. C’est pourquoi tout au long
de ce travail de recherche je me suis entretenue avec plusieurs
acteurices au sujet de l’alternatif afin de découvrir les différentes interprétations de ce mot. C’est une question que j’ai choisi de poser sans trop l’introduire au préalable afin de recueillir leur propre définition, sans l’influencer. Si cette question, et surtout leurs
réponses, m’intéressent c’est qu’elles ne sont jamais les mêmes ni exactement celles que j’attendais. Puisque j’allais leur demander ce
qu’iels pensaient de la notion d’«&nbsp;alternatif&nbsp;», je devais d’abord faire en sorte de retranscrire ma propre définition. Non sans mal et sans détour, j’en suis arrivée à ceci&nbsp;: avoir une posture alternative, c’est être face à un système ou des paramètres qui ne nous conviennent pas et c'est agir pour faire autrement, prendre un autre chemin que celui qui est attendu. J’ai construit cette définition <del class="com">suite à plusieurs lectures comme le livre</del> <u class="com">suite à la lecture de (Tu ne cite qu'un ouvrage )</u> *Post Digital Print, la mutation de l’édition depuis 1894*<span class="footnote">*Post Digital Print, la mutation de l’édition depuis 1894*, Alessandro Ludovico, édition B42, traduit en 2016</span>. L’étymologie de ce mot ainsi que les nombreuses définitions que l’on peut trouver dans les dictionnaires m’ont aussi permises d’en arriver à cette définition. Enfin, échanger avec mon entourage et me confronter à leur vision, leur questionnement et parfois leur incompréhension m’a aidé dans cette réflexion.

Pour une des créatrices du collectif Les Rad!cales<span class="footnote">Les Rad!cales est un collectif engagé pour l’environnement et le design, https://lesradicales.org</span>, avoir une posture alternative c’est, entre autres, refuser des projets quand ils ne sont pas compatibles avec notre propre <u>éthique</u> pour des raisons environnementales, économiques ou sociétales. Plus radicalement, être alternatif·ve pour elle, cela peut aussi impliquer de changer de métier lorsque l’on se retrouve dans des situations qui ne correspondent pas à nos convictions. Ou si l’on ne peut pas se le permettre, c’est faire en sorte de faire évoluer le contexte comme on l’entend. Pour donner un exemple, elle ajoute lors de notre entretien&nbsp;: «&nbsp;je peux essayer de faire plus d’expositions permanentes que temporaires, de ne
pas travailler sur des expositions dans lesquelles les œuvres ont un
bilan carbone déplorable, de travailler à sensibiliser à travers des
sujets d’exposition, comme la disparition des espèces.&nbsp;». Pour elle,
être alternatif·ve c’est le fait de produire, travailler ou créer dans
des situations qu'elle juge justes et utiles.

J’ai également questionné le collectif Luuse<span class="footnote">http://www.luuse.io</span>,
designers graphiques et développeurs basés à Bruxelles. Ils me répondent d’abord que leur
travail a déjà été qualifié de graphisme alternatif, puis me donnent une première définition&nbsp;: «&nbsp;l’alternatif c’est la non-dominance, c’est
choisir d’être dans une autre voie que la dominance&nbsp;», c’est être en dehors des
normes. Pour eux, le mot «&nbsp;alternatif&nbsp;» a une dimension politique
intéressante. Au fil de notre discussion une autre définition apparaît&nbsp;: être alternatif est complètement lié à la notion d’autonomie, comme un festival qui est en totale <u class="com">autogestion (Ndbp : définition de l'autogestion)</u> serait qualifié de festival alternatif. En conclusion de cette discussion, les membres de Luuse en sont arrivés à se poser plusieurs questions&nbsp;: «&nbsp;Sommes-nous alternatifs dans notre manière de fonctionner en tant que groupe&nbsp;? Si oui, pourquoi&nbsp;?&nbsp;» ou bien «&nbsp;Existe-t-il une norme dans l’alternatif&nbsp;?&nbsp;» ou encore «&nbsp;Y a t-il différents degrés dans le registre des alternatives&nbsp;?&nbsp;». Cette réflexion prouve bien que ce terme ouvre à de nombreux questionnements sur nos façons d'être, de créer, de produire.

Pendant la table ronde, organisée par Designers Éthiques en ligne en 2020, *Les
modèles alternatifs d’entreprises de design*<span class="footnote">*Les modèles alternatifs d’entreprises de design*, Ethics by Design, table ronde, 2020, Designers Éthiques</span>, la question de ce
qu’est l’alternatif s'est posée pour ces designers. Hélène Maitre-Marchois,
membre de la coopérative Fairness qui agit pour le développement web
responsable, explique sa vision de l’alternatif&nbsp;: «&nbsp;c’est quelque chose
qui aujourd’hui n’est pas dans la pensée majoritaire mais qui correspond à une envie profonde de travailler de façon différente de
ce qui nous est proposé sur le marché<span class="footnote">*Ibid.*</span>&nbsp;». C’est choisir de travailler avec des
modèles et valeurs qui nous correspondent. Pour Solène Manouvrier
membre de l’association OuiShare, qui s’intéresse à <u>l’économie
collaborative</u> dans le numérique, être «&nbsp;alternatif&nbsp;» c’est «&nbsp;une envie
forte de faire différemment, c’est bousculer, questionner, ne pas
accepter des choses qui existent dans la société et qui ne nous semblent
pas normales<span class="footnote">*Ibid.*</span>&nbsp;». Dans leur association, les valeurs alternatives
principales sont celles de la collaboration et de l’ouverture. Enfin,
d’après Thomas Thibault, membre du collectif BAM, qui tend à donner
l’autonomie pratique aux individus et qui travaille sur les questions
liées au numérique et à l’écologie, iels n’ont pas vraiment la
volonté d’avoir une pratique alternative, mais plutôt de faire d’une
meilleure manière&nbsp;: «&nbsp;On cherche la radicalité, plutôt que le côté
alternatif<span class="footnote">*Ibid.*</span>&nbsp;».

Nous reviendrons à cette notion de radicalité plus tard, mais ce qui est
sûr, lorsque je lis ces définitions, c’est qu’il existe de nombreuses
manières d’être alternatif·ve. Cette notion ne peut avoir une seule et
même définition, elle est un concept que chacun·e s’approprie en
fonction de sa pratique et de son point de vue. Nous devons néanmoins garder en tête que ce terme est parfois utilisé par certaine·s designers sans qu'iels ne le soient vraiment, souvent pour se donner une image progressiste. Je parle ici de groupe de personnes qui se décrivent comme alternatif·ve tout en faisant perdurer des modes de fonctionnement obsolètes qu'ils soient économiques, écologiques, politiques ou sociaux.

Comme je l’ai écrit
précédemment, l’étymologie ainsi que les définitions du mot «&nbsp;alternatif&nbsp;» m’ont permis de créer une définition propre à ma pratique
et à mes intérêts. En regardant sur le site du CNRTL<span class="footnote>Alternatif, CNRTL (consulté le 8 mai 2021), [en ligne]</span>, la première
définition de ce mot est&nbsp;: «&nbsp;Qui vient tour à tour, qui se répète à intervalles
plus ou moins réguliers&nbsp;». Aucune des définitions proposées sur ce site
ne correspondent à ce que l’on pourrait entendre par «&nbsp;alternatif&nbsp;» en termes de
création. Sur le site Larousse, plusieurs définitions me paraissent faire écho à mes recherches. Prenons la première&nbsp;: «&nbsp;Qui présente ou propose une alternative, un choix entre deux solutions<span class="footnote">Alternatif, Larousse (onsulté le 8 mai 2021), [en ligne]</span>&nbsp;». Cette
définition me pousse à chercher plus précisément le sens de «&nbsp;alternative&nbsp;». Il s’agit d’une «&nbsp;Obligation de choisir entre deux
possibilités, d’un dilemme<span class="footnote">*Ibid.*</span>&nbsp;». La deuxième définition du mot «
alternatif&nbsp;» étant&nbsp;: «&nbsp;Qui propose de concevoir autrement le système de
production et de consommation&nbsp;» et la troisième&nbsp;: «&nbsp;Qui constitue une
solution de remplacement&nbsp;». À la lecture de ces définitions ma réflexion se précise. Ces définitions traduisent toujours la possibilité
d’emprunter plusieurs chemins mais surtout de faire un choix, voire
d’être dans l’obligation de faire ce choix. La deuxième définition
comporte le mot «&nbsp;autrement&nbsp;», pour cela elle me paraît plus
intéressante. Il s’agirait de faire différemment, de prendre un autre
chemin que celui qui serait attendu. Quant à la dernière, elle parle de
solution de remplacement, une fois de plus ce serait l’idée de faire
autrement. <del class="com">Mais le terme utilisé ne me convient pas puisqu’il faut
selon moi, si l’on veut avoir une posture alternative, choisir de faire ce choix de
«&nbsp;l’autrement&nbsp;» et en être convaincu.</del><u class="com">Pas utile</u>

Pour poursuivre cette analyse des définitions du terme «&nbsp;alternatif&nbsp;», il me
semble utile de parler de ce que sont les mouvements alternatifs.
Puisqu’une alternative est un choix entre deux possibilités, le
mouvement alternatif comme on l’entend, serait de faire le choix du
non-conforme et non pas du dominant. Il existe de fait beaucoup de projets
alternatifs&nbsp;: créations, festivals, pédagogies, lieux de vie, etc.

Les mouvements avant-gardistes apparaissent au 20<sup>e</sup> siècle suite à la
première guerre mondiale et à l’effondrement des croyances envers la
Révolution Industrielle<span class="footnote">*Culture Graphique, une perspective de Gutenberg à nos jours*, Stéphane Darricau, édition Pyramyd, 2014</span>. Ce conflit mondial sera souvent abordé
dans les créations de Filippo Tommaso Marinetti, le fondateur du
mouvement Futuriste. Ce mouvement a bouleversé l’histoire de la page
imprimée, que l’on avait l’habitude de voir organisée et équilibrée. Pour Stéphane Darricau,
Marinetti «&nbsp;privilégie la tension, l’expressivité et une certaine “esthétique du désordre”<span class="footnote">*Ibid.* p.107</span>&nbsp;». Le premier manifeste
futuriste *Zang Tumb Tumb*¨ signé Marinetti et réalisé en 1914 en est un
bon exemple. Ces manifestes qui abordent principalement les thèmes de la
guerre et de la crise économique chamboulent la mise en page
traditionnelle avec une toute nouvelle approche de la typographie.

En même temps, les membres du groupe Dada «&nbsp;se proposent de
détruire toutes les conventions afin d’exposer au grand jour leur
vacuité et leur absurdité&nbsp;<span class="footnote">*Ibid.* p.108</span>&nbsp;». Ce mouvement apparu en 1916 à Zurich
dénonce radicalement les anciennes valeurs de l’art conservateur, patriotique et catholique ainsi que celles du rationalisme et du formalisme. Les compositions sont
anarchiques&nbsp;: informations en désordre, typographies et sens de lecture
multiples sur une même composition, superposition, etc. La
lisibilité traditionnelle est mise à rude épreuve, les compositions deviennent un grand
chaos, comme par exemple l’affiche *Kleine Dada Soirée*¨ réalisé par Kurt
Schwitters et Théo van Doesburg en 1922. Hannah Hoch, artiste
plasticienne Dada, fait partie des personnes emblématiques de ce
mouvement. Elle sera une
des premières de ce courant à s’intéresser au photomontage. Dans sa composition *Da Dandy*¨ (1919) elle fait une «&nbsp;
critique féministe des représentations médiatiques&nbsp;» pour montrer «&nbsp;le
caractère construit, donc artificiel, de l’identité féminine dans les
années 1920<span class="footnote">*Ibid.* p.110</span>&nbsp;».

C'est après le traumatisme de la Première Guerre mondiale que le mouvement Moderniste est
apparu avec pour objectif de créer une société nouvelle. Or, ce courant également appelé «&nbsp;Universalisme&nbsp;» a fait l'objet de nombreux rejets. Les compositions parfaitement
structurées, comme le tableau de Piet Mondrian *Composition en Rouge, Jaune, Bleu et Noir*¨, ne font pas l’unanimité. Nombreux·ses sont celleux
qui contestent le modernisme qui luttait pour des pratiques créatives
débarrassées des particularismes culturels et nationaux. Le modernisme
représente une tendance qui souhaite créer la standardisation en art, architecture et graphisme, qui a de fait engendré une uniformité. Ces courants
modernistes ont certainement fait naître chez certain·es cette volonté
d’opter pour le non-conforme et d'adopter une posture alternative dans la création. Il y a eu une «&nbsp;résistance à l’hégémonie moderniste<span class="footnote">*Ibid.* p.175</span>&nbsp;»
avec entre autres le recours à l’<u>éclectisme</u>, la diversité des sources
d’inspiration recyclées et combinées. Les Pin Push Studios inventent une
esthétique «&nbsp;bricolée<span class="footnote">*Ibid.* p.176</span>&nbsp;». Dans les années 1970, c’est la culture Punk et le
mouvement <u>*Do It Yourself*</u> qui apparaissent, toujours avec la volonté
d’une création alternative, mais aussi autonome, et dont les réalisations sont radicalement non-professionnelles et
approximatives. En France, cette dimension doit beaucoup aux événements
de mai 68. Pour de nombreux·ses artistes, il s’agira de transgresser les
règles. Par exemple, Tibor Kalman réalise la charte graphique du
restaurant *Florent*¨ , (très en vogue à l’époque) en «&nbsp;jetant&nbsp;» des
lettres en plastiques sur un tableau. Ou encore le graphiste Weingart
Wolfgang, toujours dans l’idée de s’affranchir des normes du modernisme,
réalise en 1970 *Poster Kunstkredit*¨ et transgresse les codes
typographiques.

Comme vu précedemment, la création en réaction à des normes traditionnelles, <del class="com">opposées à nos valeurs</del> <u class="com">opposées aux valeurs établits</u>, existe depuis plus d'un siècle. La tendance moderniste tentait
d’imaginer une pensée universaliste, mais celle-ci a souvent
reçu des critiques radicales alternatives. Tous ces mouvements à contre-courant ont
encouragé la conception de nouvelles esthétiques et de créations
alternatives.

#### Les origines de l'alternatif&nbsp;: <br> premières tentatives de reconquête des moyens de production

Pour Alessandro Ludovico, chercheur, enseignant et artiste italien,
le mouvement alternatif en design serait en lien direct avec l’histoire de
l’objet imprimé. Si l’on s'attarde sur les premiers projets d'édition qui ont été
qualifiés d’alternatifs, beaucoup avaient l’ambition de se réapproprier
les moyens de production. À une époque où le principal moyen de diffuser
des textes supposait de passer par l’imprimerie traditionnelle,
certain·es éditeurices ont choisi de faire autrement. L’auto-édition a
alors vu le jour. Les débuts de l’alternatif coïncident avec le
moment où l’imprimerie traditionnelle est devenue le moyen de production
à éviter. L’auto-édition permettait de diffuser plus rapidement et en
plus grande quantité des messages, souvent politiques. Certes les textes
étaient plus courts que les manuscrits édités en imprimerie
traditionnelle, mais ils étaient aussi plus facilement transportables et
diffusables.

En 1910, Marinetti monte au sommet de la Tour de l’Horloge à Venise pour
jeter des tracts, sur lesquels sont imprimés son Manifeste sur la foule
massée en bas<span class="footnote">*Post Digital Print, la mutation de l’édition depuis 1894*, Alessandro Ludovico, édition B42, traduit en 2016</span>. De nouvelles manières de créer et de communiquer un
message s’inventent, ce sont les débuts de l’édition alternative. Ainsi, le futurisme italien est «&nbsp;bien décidé à
imposer une rupture radicale avec le passé<span class="footnote">*Ibid.* p.37</span>&nbsp;». Ces nouvelles
manières d’imprimer permettent aussi aux artistes de se ré-approprier le
média qu’est l’édition. Le mouvement Dada en est aussi un bon exemple.
En expérimentant la presse à imprimer, de nouvelles formes d’éditions
apparaissent&nbsp;: taille de caractère disparates, caractères typographiques
utilisés comme élément graphique, collages, photomontages, etc. Cette
manière de créer permettait bien-sûr d’éviter toute forme de censure,
puisque les auteurices étaient les seul·es à interagir sur ces objets
avant leur distribution.

De nouveaux outils avaient déjà permis d’ouvrir le champ des possibles en
termes d’édition alternative. Par exemple, la <u>machine miméographique</u>¨ de Thomas Alva Edison en 1870. Cette machine peu coûteuse et légère à été
adoptée par de nombreux·ses artistes. C’était l’outil d’impression
clandestine idéal «&nbsp;assez léger et compact pour être transporté
facilement d’un endroit à un autre, permettant ainsi d’échapper à la
confiscation et à la censure<span class="footnote">*Ibid.* p.40</span>&nbsp;». Cette machine permettait aussi de
diffuser hors des circuits traditionnels&nbsp;: les premiers <u>fanzines</u> de
science-fiction sont alors apparus aux États-Unis pendant les années 50.
Puisque le miméographe permettait de sortir des circuits traditionnels
de distribution, il a permis entre autres, de diffuser des
textes alors interdits par le gouvernement. Peu d’exemplaires étaient
distribués et ceux qui les recevaient étaient censés les reproduire pour
à leur tour les diffuser. Cette manière de diffuser était une
alternative à ce qui se faisait alors. Une fois que
l’auto-édition est devenue accessible, c’était au tour de la diffusion
de devenir alternative. Nous aborderons ce sujet plus tard.

Après le miméographe, c’est la photocopie qui est apparue pendant les
années 1960. Ainsi, publier un fanzine ou imprimer un flyer n’avait jamais été
aussi simple. La <u>réappropriation</u> des médias et la liberté d’expression a
été rendue possible grâce à cette technique. Le courant *Do It
Yourself* a complètement séduit le mouvement punk, qui a beaucoup utilisé
cette technique.

Grâce à ces nouvelles techniques, et bien d’autres que je n’ai pas
citées, les artistes et designers <del class="com">ont obtenu</del><u class="com">ce sont emparer (ex: Sniffin Glue, l'auteur s'est emparé de la photocopieuse de la banque dans laquelle il travaillait)</u> la possibilité de contrôler
leur projet du début jusqu’à la fin, de l’idée jusqu’à la diffusion. La
création alternative serait donc complètement liée avec l’autonomie&nbsp;: il
s’agit de créer et diffuser sans passer par les systèmes dominants, de les transgresser, parfois pour éviter la censure, tout en contrôlant le plus de choses possible par soi-même (ou dans un réseau choisi).

#### S'émanciper des contraintes&nbsp;: <br> comment repousser les limites des systèmes de production dominants

Les fonctionnements alternatifs peuvent exister dans d’autres domaines
que celui de la création. Par exemple, lorsque l'on choisi de travailler dans des lieux alternatifs, d'être alternatif·ve dans la façon d’échanger des
services ou encore dans sa manière de fonctionner au sein d’une équipe.

Commençons par aborder les lieux de travail qui pourraient être
qualifiés d’alternatifs. Les designers peuvent travailler dans des
locaux dédiés uniquement à l’entreprise ou l’agence auxquelles iels
appartiennent. Un·e designer peut aussi travailler dans un lieu
associatif où d’autres activités y sont pratiquées, comme iel peut
travailler depuis chez soi. Ça n’est pas toujours le cas, mais lorsque
c’est possible les designers choisissent leur lieu de travail en
fonction de leur morale. Étienne Ozeray, membre de Luuse, me disait&nbsp;: «&nbsp;Un atelier c’est
un outil aussi, dans le sens où, c’est un endroit qu’on utilise et on
doit avoir la même posture avec notre espace de travail qu’avec notre
ordinateur&nbsp;». Idéalement, en tant que designer, nous devrions choisir notre lieu de
travail comme nous choisissons les personnes avec qui nous travaillons
ou les projets que nous réalisons. Pour certain·es, travailler chez elleux leur permet d'être plus indépendant·e, et donc autonome, vis-à-vis de certains systèmes financiers. Pour d'autres, travailler dans un lieu partagé leur permet de poursuivre leur démarche de coopération déjà entamée dans leur travail. Celleux qui choisiront de travailler dans un lieu occupé, comme un squat, pourront construire un lien fort entre la création autonome, libre, et leurs lieux de travail.

<del class="com">Il me semble que</del> l’ASBL<span class="footnote">Association Sans But Lucratif présente à Bruxelles, https://communa.be</span> bruxelloise Communa est un bon
exemple de ce que pourrait être un lieu de travail alternatif. Ce lieu
appartient au mouvement des squats, dans lequel un collectif décide
d’occuper un lieu inhabité. On peut lire sur Wikipédia<span class="footnote">Squat, Wikipédia (consulté le 22 juin 2021), [en ligne]</span> qu’il s’agit
souvent de décisions prises par des personnes en accord avec le
<u>Mouvement autonome</u>. Il s’agit d’expérimenter l’autogestion dans de
nouveaux lieux. Plusieurs collectifs d’artistes et de designers y
travaillent&nbsp;: Luuse, Jeudi AM, Cy.clones, Pouvoir Faire, etc.

Communa rend possible l’occupation temporaire de lieux non utilisés, plus ou moins vastes. Dans ces bâtiments vides, elle propose deux choses
&nbsp;: des logements pour des personnes dans le besoin ainsi que des espaces
de travail, d’exposition, de création et donc de rencontre. Puisque ces
espaces sont nouveaux, ils en deviennent expérimentaux. Les personnes
occupantes, ont pour différentes raisons, le besoin et l’envie d’occuper
ces lieux, une vraie synergie s’y crée. Quelque chose d’important se
passe dans la gestion de ces lieux de vie&nbsp;: chacun·e se sent responsable et impliqué·e
dans son bon fonctionnement. Tous les mois des assemblées générales ont
lieu, entre les «&nbsp;habitant·es&nbsp;» du lieu, afin de repenser le modèle
d’organisation. Les décisions sont prises en commun, toutes les
nouvelles informations relatives au lieu sont partagées via un réseau
social, enfin l’entretien des locaux est géré à tour de rôle par
les occupant·es. Ces espaces font naître entre les habitant·es un
rapport de partage&nbsp;: iels mettent en commun leur savoir-faire.

Il en découle une nécessaire autogestion, où chacun·e apporte ses
compétences au projet (travaux, communication, entretien, etc). Cette
mutualisation entraîne très souvent de nouveaux échanges non-monétaires,
sous forme de troc. Puisque de nombreuses associations y cohabitent, elles ont la
possibilité d’échanger des services au lieu d’aller demander un service
à des personnes tierces contre de l’argent. Certain·es donnent de leur
temps pour les travaux de rénovation du lieu, d’autres aideront à la
communication ou partageront leur matériel. Enfin une participation
financière est demandée aux habitant·es pour couvrir les frais de
l’occupation, celle-ci s’adapte en fonction des moyens de chacun·e. Ces
lieux sont également ouverts au habitant·es du quartier, dans lesquels
beaucoup d’événements ont lieu&nbsp;: tournoi de foot, projection de film,
concert, etc.

Ces lieux de travail et de vie peuvent être considérés comme alternatifs dans le sens
où les «&nbsp;habitant·es&nbsp;» ont choisi d’emprunter un chemin différent. Ils ne sont ni un
nouvel espace de coworking où chacun·e y viendrait dans le seul but de
travailler, ni une sorte de résidence avec de nombreuses habitations.
Ils sont des lieux de vie où le commun prime sur tout le
reste, un endroit où cohabitent différentes personnes qui ne se seraient
certainement jamais rencontrées. Ces espaces sont également alternatifs
dans le sens où ils ont pris vie en réaction à une situation critique pour les créateurices de l’ASBL&nbsp;: le nombre important de
personnes ayant besoin d’un toit ou de locaux alors qu’il existe
beaucoup de bâtiments vides. <del class="com">Évidemment, nous pourrions nous poser la question de leur «&nbsp;légitimité&nbsp;» à être reconnu comme alternatif si nous les comparons à des lieux comme de véritables squats. En fait, il existe toujours des postures plus alternatives que d'autres. Il me semble alors que le plus important est d'être complètement engagé·e envers celle que nous choisissons.</del> <u class="comm">Je ne sais pas si c'est un commentaire pertinent, ne te dresse pas en juge morale pour savoir ce qui est plus alternatif</u>

Dans le cas de Luuse, leur choix quant à leur lieu de travail s’est fait
en adéquation avec leur posture de travail. Puisqu’ils créent avec des
notions telles que la transmission, le partage de savoir-faire ou encore
la constitution de communs, les membres ont choisi de rejoindre Maxima,
un des bâtiments proposés par Communa. Il ne s’agit plus de travailler,
seul·e, dans un espace dédié uniquement à leur pratique et où les
rencontres seraient planifiées et avec des personnes choisies. Tout un
écosystème se crée dans cet espace, dans lequel il existe nombre
d’identités et de savoir-faire différents, avec la volonté pour chacun·e
de les partager. Ces espaces partagés supposent d’acquérir un tout
autre fonctionnement pour les designers&nbsp;: iels sont sur le terrain,
constamment dans l’échange et le partage.

C’est également pendant mon stage dans ce lieu que j’ai pu être
confrontée pour la première fois à des échanges de services alternatifs
&nbsp;: les échanges non-monétaires. Cette manière de procéder me fait penser
au texte de Bob Black, publié en 1985 *Travailler, moi&nbsp;? Jamais*<span class="footnote">Travailler, moi&nbsp;? Jamais, Texte de Bob Black, écrit en 1985, Les éditions du libre #1</span>&nbsp;: en faisant celà, n’est-ce pas
s’écarter du travail dans son sens aliénant&nbsp;? Pour cet auteur «&nbsp;Le
travail est la production effectuée sous la contrainte de moyens
économiques ou politiques \[\...\]&nbsp;». Dans le cas de l’échange
non-monétaire les contraintes économiques n’existent plus, il s’agirait
alors d’échanges de services alternatifs et non plus de travail à
proprement parler. Lorsque je pose la question aux membres de Luuse de
ce qu’est l’alternatif pour eux, une de leur réponse sera justement
cette question du troc&nbsp;: réaliser un site contre l’entière collection des livres
d’une maison d’édition, ou encore contre une autre production jugée de valeur équivalente par accord préalable. Il est de ce fait non-négligeable de comprendre que cet échange non-monétaire n'est pas toujours simple, puisque il faut d'abord trouver cet accord.

Ce moyen d’échange de service alternatif se rapproche des projets de
monnaie locale. Dans ce cas, il s’agit bien d’échanges monétaires, mais c’est
une monnaie qui peut être qualifiée d’alternative, et celà pour
différentes raisons. Il existe des projets de monnaie locale dans
plusieurs endroits en France, mais prenons l’exemple de l’*Eusko*&nbsp;: la
monnaie locale du Pays-Basque lancée en 2013. Le cofondateur de
cette monnaie, Dante Edme-Sanjurjo, la présente comme «&nbsp;un projet politique et
écologique<span class="footnote">«&nbsp;Eusko, monnaie locale, exemple de design de politique publique&nbsp;» - Dante Edme-Sanjurjo - Ethics by Design, [en ligne]</span>&nbsp;». Cette monnaie utilisée sur tout le territoire du
Pays-Basque, est née d’un projet militant de préservation de la
culture basque. Il explique que cette monnaie n’a pas
forcément pour objectif de remplacer l’Euro mais de le compléter.
L’*Eusko* permet de dynamiser les échanges locaux et donc de favoriser les
<u>circuits courts</u>, c’est là que l’enjeu écologique de cette monnaie
alternative entre en jeu. Pour elleux, il s’agissait entre autre de «
démondialiser pour limiter les émissions de gaz à effet de serre&nbsp;».
Grâce à cette monnaie locale, une «&nbsp;relocalisation de l’économie&nbsp;»
devient possible. Pour les fondateurices de l’Eusko la monnaie est avant
tout un outil social qui crée du lien et qui permet de porter
collectivement un projet. Bien plus qu’un simple outil économique, elle permet une dynamique citoyenne. Elle crée du commun, autour d’une
cause qui peut-être de l’ordre du social, de l’écologie ou du politique.
<u class="com">Explique le fonctionnement économique de l'Eusko</u>

Enfin, est-il possible d'être alternatif·ve dans notre mode de
fonctionnement, à travers notre statut par exemple, pour échapper à
certaines normes&nbsp;? Il me semble que certains statuts sont alternatifs
par rapport à d’autres&nbsp;: par exemple, être un collectif est alternatif
par rapport au fonctionnement d’une agence de publicité. Mais est-ce
vraiment alternatif puisque beaucoup de designers utilisent ce statut&nbsp;?
Peut-être que la forme associative est la forme la plus alternative pour
des designers graphiques, car c’est celle qui s'éloigne le plus des
aspects commerciaux, économiques, sociaux et politiques, habituellement
assimilés au graphisme. Très souvent, le design
graphique est trop rapidement associé à la société de consommation. En effet, si l’on
regarde la définition sur le site de Pôle Emploi, on peut lire&nbsp;: «&nbsp;Le
designer graphique associe son sens artistique aux techniques de
communication et de marketing afin de mieux séduire les
consommateurs&nbsp;».

Avoir le statut associatif permet d’abord d’entamer des projets de
recherche en étant financé·e, plus seulement à travers un rapport commercial à
des client·es, mais grâce à des <u>subventions</u> de l’État, à des dons, des ventes ou à des adhésions. Des projets auto-initiés sont
possibles, pour lesquels il n’y a plus de dépendance au commanditaire. Une association est un objet social non lucratif, c'est-à-dire qu'elle peut participer à l'économie mais ne recherche en aucun cas le profit, contrairement à une entreprise. De plus,
une association doit avoir un caractère, une finalité sociale. Les
designers doivent alors pouvoir définir ce qu’est leur finalité sociale,
c’est-à-dire quelle responsabilité sociale iels souhaitent porter, mais
aussi quelles actions iels mettront en place pour arriver à cette
finalité. C’est une manière de définir ses responsabilités, son utilité
et ses engagements.

Le choix d’un lieu de travail alternatif, les statuts utilisés ou encore
l’utilisation d’échange non-monétaires ou de monnaies alternatives,
donnent la possibilité de s’émanciper des contraintes imposées par les
systèmes dominants. Ces choix demandent un engagement, voire une
certaine radicalité dans la façon de travailler. Entre la notion
d’alternative et la radicalité un lien évident existe&nbsp;: celui de faire
un choix, un choix parfois à contre courant, mais un choix engagé.

### 2. Radicalité et Design

#### Pour une radicalité transformatrice&nbsp;: <br> rupture avec l'ordre établi

Le mot «&nbsp;radical&nbsp;» souvent associé aux postures radicales, vient de «&nbsp;racine&nbsp;» par son étymologie. Être
radical·e ça veut donc dire revenir à la racine de quelque chose, aller
à l'origine des choses et ne pas rester dans le flux ou en surface. Ce mot, dans notre
société et son contexte politique actuel est souvent lié à la violence, au
terrorisme (radicalisation, dé-radicalisation). J’aimerais au contraire
penser ce mot comme la possibilité de faire naître des énergies
créatrices, être radical·e dans sa manière de créer, de produire, c’est refuser
lorsque l’on est en désaccord, c’est aussi proposer des
alternatives, imaginer différemment, etc.

La philosophe Marie-José Mondzain a voulu redonner tout son sens au mot
radical. Le détonateur pour son intérêt envers ce terme a été le projet
dit de dé-radicalisation, très abordé dans les médias pendant les attentats terroristes. On a parlé de dé-radicalisation comme retour à
la normalité et c’est une erreur d’après elle. La radicalité ne veut pas dire être dans la norme, c’est tout le contraire, c’est une rupture
avec l’ordre établi. Marie-José Mondzain considère que&nbsp;: «&nbsp;Si le mot
radical veut dire quelque chose, c’est en rompant avec sa propre racine,
le mot «&nbsp;racine&nbsp;», ce n'est pas pour défendre un enracinement quelconque
c’est pour parler au contraire d’un régime de rupture, de nouveauté, de
création et de transformation<span class="footnote">«&nbsp;Marie-José Mondzain, Une philosophe «&nbsp;radicale&nbsp;»&nbsp;?&nbsp;», La Grande table, France Culture, 2017, [en ligne]</span>&nbsp;». Être radical·e c’est sortir d’une
vision normative pour aller vers la surprise, vers l’ouverture des
possibles.

D’après elle, le monde de la création est l’endroit le plus approprié
pour être radical·e. Puisque nous créons des idées, des formes, des
évènements, nous pouvons proposer et mettre en œuvre un monde différent.
L’urgence est de réfléchir aux moyens pour faire bouger les choses, aux
nouvelles formes de luttes. Elle explique aussi que les structures de
communication et l’ensemble des systèmes financiers, même dans la culture, paralysent l’arrivée du nouveau. Par exemple, les
tentatives d’autonomisation (lieux de vie, pédagogie, etc) sont souvent
réfutées par les gouvernements ce qui tend à provoquer chez beaucoup la
peur de la nouveauté. Les médias jouent un rôle dans la croyance que la
nouveauté n’est jamais bonne, par exemple lorsqu’on regarde ou écoute «&nbsp;les nouvelles
» et qu’elles n’apportent très souvent que des mauvaises nouvelles.

Marie-José Mondzain prend très souvent l’exemple des saxifrages qui veut
dire par son étymologie «&nbsp;casser la pierre&nbsp;». Ces plantes poussent sans
racines, se reproduisent grâce au vent et poussent partout, entre les
pierres, entre les pavés. L’image de cette plante qui pousse partout,
vite et sans avoir besoin de racines, contredit la croyance du besoin
d’enracinement identitaire que nous pourrions avoir.

La radicalité en design peut prendre de nombreuses formes en fonction de
nos intérêts. En design graphique, sujet sur lequel je suis le plus apte
à parler, les questions environnementales, financières, politiques et
sociétales sont des sujets dans lesquels nous pouvons et avons tout
intérêt à être radicaux et radicales. Évidemment, il est impossible
d’être sur tous les fronts, mais il me semble néanmoins que nous devons
être au moins informé·es. Par exemple, les projets d’autonomisation
financière, par le choix de ne travailler qu’avec du <u>logiciel
libre</u> ou bien de travailler contre des échanges
non-monétaires en sont un bon exemple. Dans le monde de l’édition,
beaucoup de questionnements existent autour de l’environnement&nbsp;: quel
papier utiliser ou quel support pour remplacer le papier, quel
type d’encre, acheter du papier ou récupérer des supports non-utilisés,
etc...

Pour aborder ce sujet de la radicalité dans la création, la thèse de Daria Ayvazova, aujourd’hui designer, *Le designer
défroqué*<span class="footnote">Daria Ayvazova, «&nbsp;Poïétiques du design 3&nbsp;», Journées d’études doctorales en design, Université de Strasbourg, 2015, [en ligne]</span>, me semble pertinante. Lorsque Daria Ayvazova présente sa thèse, elle commence
par donner une définition du mot «&nbsp;défroqué&nbsp;»&nbsp;: c’est un religieux qui
doute au point de renoncer. Par exemple, un prêtre défroqué est celui
qui renonce à une doctrine, qui n’y croit plus et qui ne se met plus à son service. Elle met donc en lien la religion et le
capitalisme qui serait une nouvelle religion, considérant qu’il y a dans
les deux systèmes des points communs&nbsp;: rîtes, croyances, idolâtrie, etc.
Beaucoup de designer adhèrent à cette nouvelle religion en créant le
produit ou la communication qui seront appréciés et achetés par les
consommateurices. Ces designers ont un rôle prépondérant dans le système
capitaliste. Le design est donc bien un outil politique fort et les
designers ont un rôle politique indirect. La culture de
l’hyper-consommation ne laisse que peu de place à l’expérimentation
ainsi qu’à l’information sur l’organisation des systèmes et outils mis
en place.

Or certain·es designers ont choisi d’être critique face à ce système capitaliste,
ou en tout cas d'agir en contradiction avec celui-ci. Je les nommerai les
designers radical·es, Daria Ayvazova les appelle les designers
défroqué·es. Ces designers remettent leur discipline en question et semblent apporter des solutions alternatives. Pour elle&nbsp;: «&nbsp;Le designer défroqué sera capable d’élaborer des dispositifs inédits pour braver la crise
actuelle en adoptant un comportement parfois subversif pour repousser
les limites par ses actions expérimentales et contestataires<span class="footnote">*Ibid.*</span>&nbsp;».
Daria Ayvazova cite également trois points décisifs dans le travail de
ces designers, le premier&nbsp;: établir interaction et cohésion sociale
grâce à la participation ou la coopération, ce qui rappelle l’idée de
monde commun à construire dont parle Marie-José Mondzain. Le deuxième
point vise à établir un modèle économique alternatif fondé sur
l’autonomie pour la création. Une fois de plus, c’est l’autonomie qui
revient comme solution pour sortir de notre système actuel. Et pour
finir&nbsp;: concevoir le design comme pratique critique et indépendante,
utilisée comme autodéfense contre l’érosion de la réflexion et de la
<u>pensée critique</u>. L’objectif des designers défroqué·es est d’encourager
les usager·ères à l’autonomie et la réflexion, entre autres, en proposant un
rapport social et un pouvoir horizontal. Les usager·ères ont à nouveau la
possibilité et l’espace nécessaire pour avoir une pensée critique sur
les objets et images proposées. Marie-José Mondzain précise&nbsp;: «&nbsp;Il y a
les images qui donnent la parole et les images qui prennent la parole<span class="footnote">«&nbsp;Marie-José Mondzain, Pour une radicalité vivante&nbsp;», France Culture, 2017, (consulté le 20 juin 2021), [en ligne]</span>&nbsp;». Des œuvres d'art contemporain, par exemple, peuvent-être des «&nbsp;images qui donnent la parole&nbsp;», puisqu'elles ouvrent à la discussion, à l'imagination. Les productions des designers radical·es, ou défroqué·es, proposent des images qui donnent la parole, contrairement à la publicité
ou à la propagande, par exemple, qui empêchent tout dialogue et ne le
cherchent d’ailleurs pas. Le même principe existe aussi pour les
dispositifs de diffusion, certains ouvrent à la discussion, les évènements publics par exemple, contrairement à d’autres comme la télévision.

Toutes ces notions ne sont pas étrangères au
Design Radical italien. Ce mouvement, artistique et architectural, très
similaire au mouvement de l’<u>Anti Design</u> a émergé dans les années 1960,
en réaction au courant Moderniste. Le Modernisme luttait pour un design
<u class="com>«</u>lisse et parfait<u class="com>» (c'est ton interprétation, peut être trouve une citation critique...)</u>. En agissant de la sorte, les designers vont créer
une norme en déterminant ce qui est convenable ou non en matière de
design. Tout ce qui ne rentrait pas dans cette norme était qualifié de
mauvais design. Le modernisme a créé un mécanisme dans lequel les
autres esthétiques ont été mises de côté, comme nous l’avons
vu précedemment.

Le Design Radical est donc un «&nbsp;courant contestataire, qui ouvre
l’architecture à des pratiques conceptuelles et artistiques, affranchies
de toute finalité constructive<span class="footnote">Architecture Radicale, FRAC Centre-Val de Loire (consulté le 26 aout 2021), [en ligne]</span>&nbsp;». Ce mouvement choisit de replacer
l’usager·ère au centre du projet architectural, par le biais d’installations
ou encore de performances dans la rue. Le design devient un débat
public, notion que l’on peut retrouver autant dans le discours de
Marie-José Mondzain que dans celui de Daria Ayvazova. En refusant
les aspects consuméristes et normatifs du courant moderniste que
prolonge la société actuelle, le Mouvement Radical italien va créer du
sensible, du poétique, de l’utopique et donc de l’imaginaire. Membre de
ce courant, Ettore Sottsass, architecte, designer industriel et
expérimental, a remis en question le rôle des designers&nbsp;: «&nbsp;J’estimais que
le rôle de l’architecte ou des designers, à ce moment-là, était
d’instaurer une méthodologie du doute, de la souplesse, de la
construction/destruction, de la gravité/ironie, de
l’optimisme/pessimisme, de la forme/non forme, etc.<span class="footnote">Ettore Sottsass, Index Grafik, (Consulté le 26 aout 2021), [en ligne]</span>&nbsp;»

Être radical·e en design c'est aussi refuser l'aspect consumériste et normatif pour s'intéresser à la finalité sociale d'un projet. Les artistes radicaux et radicales ne cherchaient pas le profit mais l'autonomie, tout en osant la nouveauté pour trouver des solutions. Mais plus que ça, être radical·e c’est une prise de position engagé·e&nbsp;: «&nbsp;On ne peut connaître des valeurs sans se donner à
elles<span class="footnote">*Réflexions sur l’engagement personnel*, Paul-Louis Landsberg, Revue Esprit n°62, 1937. Ré-édition en 2021, Allia</span>&nbsp;».

#### L’engagement

L’engagement est une des principales postures qui permettent à de tels projets radicaux d’aboutir. S’impliquer intégralement dans
un projet, s’en emparer, voire l’habiter, sont des moyens de trouver des
réponses efficaces. Dans le livre *Réflexions sur l’engagement
personnel*<span class="footnote">*Ibid.*</span>, Paul-Louis Landsberg propose une définition de
l’engagement&nbsp;: «&nbsp;Nous appelons l’engagement l’assumation concrète de la
responsabilité d’une œuvre à réaliser dans l’avenir, d’une direction
définie de l’effort allant vers la formation de l’avenir humain<span class="footnote">*Ibid.* p.10</span>&nbsp;». Ce texte a été écrit en 1937, deux ans avant la Seconde Guerre mondiale. Dans une époque, où se mêlent l'incompréhension, la violence et la peur dues, entre autres, à la montée de nouvelles dictatures en Europe, Paul-Louis Landsberg écrira ce texte. Celui-ci est toujours d'actualités, il n'est pas compliqué de se retrouver dans ces mots et c'est ce que nous allons faire ici.

Lorsque j’aborde la radicalité, je parle évidemment de celle-ci dans
le registre du design, plus précisément du design graphique, mais il est
évident que je pourrais facilement l'envisager dans un registre bien plus
large&nbsp;: celui de la société et de son développement. Les problématiques
que nous pouvons rencontrer en design graphique, que j’ai énumérées plus
haut, s’appliquent également à notre société en général&nbsp;:
l’environnement, l’économie, le politique, etc. Être engagé·e c’est
aborder un sujet à bras le corps, y faire face et trouver les idées pour
y réagir. Pour trouver ces idées, l’imaginaire est l’élément principal,
puisqu’il s’agit d’imaginer un monde commun, meilleur et respectueux.

Mais, ne perdons pas de vue qu’aucune cause n'est parfaite. Paul-Louis
Landsberg écrit que la valeur de l’engagement existe grâce à la tension
entre imperfection de la cause et caractère défini de cet engagement.
C’est d’ailleurs en comprenant qu’une cause n’est pas parfaite que l’on
évite le fanatisme. L’engagement doit rester libre et ne doit en aucun cas
être une confiance aveugle. La personne libre et engagée
a la capacité de faire une critique et de remettre en question certains
aspects de la cause qu’iel soutient. On ne devrait pouvoir s’engager
sans une réflexion critique, puisque selon Landsberg «&nbsp;l’engagement ne constitue pas
pour nous un glissement aveugle mais un mouvement personnel qui contient
un acte de connaissance<span class="footnote">*Ibid.* p.34</span>&nbsp;». Marie-José Mondzain porte le même
intérêt à la capacité critique de chacun·e, elle écrit&nbsp;: il y a un «
travail de réappropriation de la liberté et de la capacité de chacun
d’exercer sa puissance critique et son imagination<span class="footnote">*Confiscation des mots, des images et du temps, Pour une autre radicalité*, Marie-José Mondzain, édition Poche+, 2019</span>&nbsp;». Celleux qui
sont engagé·es sont complètement libres et peuvent alors lutter contre
les résistances qui s’opposent à leurs convictions. Pour elle, être radical·e c’est refuser de consentir aux normes mises en
place par l’ordre dominant. Ce refus fait appel au courage des ruptures
et à l’imagination la plus créative. Elle précise que la radicalité ce n’est pas la violence, contrairement à ce que l’on pourrait finir par croire à cause de la «&nbsp;confiscation&nbsp;» de ce mot<span class="footnote">*Ibid.* p.104</span>. On fait croire
que la radicalisation est un endoctrinement qui demande la soumission à
des normes et des règles, or être radical·e c’est justement celleux «&nbsp;s’engageant sur le chemin de l’émancipation, se défaisant des liens qui
les entravent<span class="footnote">*Ibid.* p.97</span>&nbsp;». Ces deux termes, engagement et radicalité sont en
fait très proches. La radicalité ne pourrait exister sans l’engagement,
mais la radicalité n’est pas qu’un simple engagement, elle est un
engagement absolu, sans exception ou atténuation.

À travers les discours et visions de ces auteurices et designers
plusieurs notions reviennent souvent, les principales restent les termes
d’expérimentation, d’imaginaire et de norme. Il me semble que l’une ne va
pas sans l’autre&nbsp;: pour expérimenter, afin d’échapper aux normes, les
créateurices ont besoin de nourrir leur imaginaire, c’est-à-dire
d’imaginer des formes alternatives.

#### L'expérimentation pour échapper aux normes&nbsp;?

L’expérimentation a plusieurs dimensions et définitions. Elle a une
portée scientifique&nbsp;: c’est une «&nbsp;méthode qui a pour but de vérifier des
hypothèses et d’acquérir des connaissances<span class="footnote">Expérimentation, CNRTL (consulté le 10 septembre 2021), [en ligne]</span>&nbsp;». L'autre définition
est celle de ressentir brutalement des émotions nouvelles et/ou innatendues qui pourrait à l’avenir servir de leçon. Expérimenter c’est à la fois faire
la preuve mais c’est aussi éprouver. Claude Bernard écrit&nbsp;: «
L’expérimentateur réfléchit, essaye, tâtonne, compare et combine pour
trouver les conditions expérimentales les plus propices à atteindre le
but qu’il se propose<span class="footnote">*Introduction à l’étude de la médecine expérimentale*, Claude Bernard, Flammarion, paru en 1865</span>&nbsp;». Il écrit également que celui ou celle qui expérimente
«&nbsp;ne devra pas craindre d’agir même un peu au hasard, \[...\] Ce qui
veut dire qu’il peut espérer, au milieu des perturbations fonctionnelles
qu’il produira, voir surgir quelques phénomènes imprévus \[...\] Ces
sortes d’expériences de tâtonnement \[...\] sont destinées à faire
surgir une première observation imprévue et indéterminée d’avance, mais
dont l’apparition pourra suggérer une idée expérimentale et ouvrir une
voie de recherche&nbsp;».

Il est essentiel de différencier l’expérimentation de l’expérience. Nous
faisons tous·tes des expériences, ce sont les effets que produit
l’environnement sur des sujets, sans forcément le vouloir. Alors que
l’expérimentation est inévitablement recherchée et mise en place. C’est
un procédé qui traite de l’expérience et qui peut provoquer de
l’expérience, mais elle ne nous tombe pas dessus. Les
expériences sont provoquées en vue d’observer les résultats<span class="footnote">*Ibid.*</span>.

Dans la proposition que je fais, c’est-à-dire d’être radical·e en tant
que designer, l’expérimentation a une place importante puisqu’elle
permettra d’acquérir de nouvelles connaissances et savoirs. Grâce à elle, nous pourrons nous confronter à des outils ou procédés alternatifs comme à des organisations alternatives. L’expérimentation
pourrait également servir à «&nbsp;vérifier des hypothèses&nbsp;» que notre
imaginaire aurait produit. L’expérimentation sera un des moyens pour
rendre des idées, qui semblent au premier abord utopiques, réalisables.
Mais l’expérimentation ne peut exister sans imaginaire, ces termes sont comme
l’engagement et la radicalité&nbsp;: complémentaires et nécessaire à l’une comme à l’autre.

Cet imaginaire est appelé par le philosophe Cornelius Castoriadis&nbsp;: «
l’imaginaire radical&nbsp;» et cette formulation sera reprise par Marie-José
Mondzain. Pour elle, l’imagination radicale doit plaider pour la
liberté, qu’elle soit individuelle ou collective. Cet imaginaire radical
permet d’expérimenter, de créer, et donc de sortir de l’impuissance, en
créant du commun.

Les normes imposées par les systèmes dominants sont souvent la raison de
cette impuissance&nbsp;: elles nous font croire qu’il n’y a pas de possible
au-delà d’elles. Elles sont tellement ancrées dans nos esprits que
beaucoup ne les questionnent plus. Or, en tant que personne engagée dans
une démarche radicale, il est essentiel d’avoir un esprit critique
envers ces normes. Il existe de nombreuses entreprises de normalisation,
qu’elles soient de l’ordre de la technique, de l’économie ou du social.

La norme est un «&nbsp;état habituel, régulier, conforme à la
majorité des cas<span class="footnote">Norme, CNRTL (consulté le 10 septembre 2021), [en ligne]</span>&nbsp;». Plus précisément, en psychologie sociale, la
norme est définit comme des «&nbsp;règles, prescriptions, principes de
conduite, de pensée, imposés par la société, la morale, qui constituent
l’idéal sur lequel on doit régler son existence, sous peine de sanction
plus ou moins diffuse<span class="footnote">*Ibid.*</span>&nbsp;». On peut comprendre que la norme est un
état dans lequel nous devons nous ranger pour éviter tout conflit. Justement, l’expression «&nbsp;Rentrer dans la norme&nbsp;» est assez
démonstrative de cette soumission que nous devons supporter pour ne pas
être en conflit avec l’ordre établi, pour se ranger dans ce qui sera vu
comme normal, acceptable. Le CNRTL donne une définition de cette
expression&nbsp;: «&nbsp;se conformer à la norme, aux règles sociales, après un
temps de libération hors norme; retomber dans la soumission à la norme,
aux habitudes et aux valeurs dominantes<span class="footnote">*Ibid.*</span>&nbsp;».

Le terme «&nbsp;hors norme&nbsp;» que nous retrouvons dans une des définitions
citées plus haut me semble intéressant. Être hors norme, c’est refuser
ce qui est inculqué comme normal et respectable. En graphisme de
nombreuses normes existent, que ce soit au niveau des formes, de l’usage
des objets mais surtout de la technique. Un des moyens pour refuser ces
normes est très certainement l’autonomie. L’autonomie, en tant qu’acte
politique engagé, permet une certaine rébellion contre l’ordre établi
tout en donnant lieu à des réponses alternatives, souvent radicales.

### 3. Autonomie et graphisme&nbsp;: un acte politique

#### Créateurices et utilisateurices autonomes&nbsp;: <br>Rendre les usager·ères acteurices

L’autonomie, dans une approche générale, peut se définir comme le «&nbsp;fait de se gouverner par ses propres lois&nbsp;». C’est la «&nbsp;faculté de se déterminer par soi-même, de choisir, d’agir librement&nbsp;».
L’autonomie peut être économique, administrative, morale,
intellectuelle, politique, financière. Dans tous les cas, c’est le fait
d’être indépendant·e d’une ou plusieurs autorités dans lesquelles on ne
se reconnaît pas, de réagir contre ces systèmes. Certain·es choisissent
de vivre en autonomie, de travailler, produire ou créer de manière
autonome. Il existe de nombreuses manières d’être autonome et tout le
monde ne peut pas être autonome sur tous les aspects. Peut-on même être
complètement autonome d’un système ou seulement en partie&nbsp;?

De quelle manière les graphistes peuvent-ils être autonomes&nbsp;? Précédemment, nous avons abordé la question du lieu de travail alternatif et des statuts qui permettent
l’autonomie, très souvent une autonomie financière. Nous nous
intéresserons plus tard à la question de l’autonomie face aux outils de création.

J’ai rencontré Thomas Thibault du Collectif BAM<span class="footnote">Le collectif BAM est un studio de design qui favorise l’autonomie,
https://www.collectifbam.fr</span>, à propos des
questions qui gravitent autour de l’autonomie. Il commence par me donner
son point de vue sur cette notion&nbsp;: «&nbsp;Il y a d’abord la question de
comment faire pour que le projet soit autonome puis la comment le projet permet aux personnes d’être autonomes&nbsp;».
Il existe, d’après lui, dans le deuxième aspect, plusieurs étapes.
D’abord rendre perceptible, c’est-à-dire la manière dont on essaie de
rendre plus visibles des choses qui paraissent cachées, imperceptibles.
Et ensuite la mise en pratique, qui permet de comprendre comment
fonctionnent ces choses-là. En design, plus précisément il s’agirait de
«&nbsp;créer un design qui n’impose pas une vision du monde mais un design
que chacun pourrait modifier, on veut créer des objets qui permettent
aux gens de se questionner&nbsp;». Voilà comment la question de l’autonomie se
pose&nbsp;: penser des objets qui permettent la
réappropriation par chacun·e, en fonction de ses connaissances et de ses outils.

Depuis plusieurs années maintenant, des artistes et designers
expérimentent autour de la question de la ré-appropriation des objets
par les usager·ères. Le *Whole Earth Catalog*¨ dirigé par Stewart Brand est une des premières tentatives concluante d’échange entre éditeurices
et lecteurices au États-Unis. Publié de façon régulière entre 1968 et 1972, ce
catalogue avait comme objectif de promouvoir l’accès aux outils. Dans ce
catalogue singulier le contenu évoluait sans cesse en fonction des
retours, des expériences et des suggestions des utilisateurices. Il y
avait ainsi une réelle interaction avec les lecteurices
puisqu’iels contribuaient directement à son contenu. C’était déjà un
outil en réseau.

C’est aussi le mouvement du *Mail Art* qui allait donner une plus grande envergure à ce
genre de projet d’utilisateurices autonomes. Puisqu’un réseau entre
artistes se créait, les éditeurices ont voulu le mettre à profit. Steve
Hitchcock, à la fin des années 70, envoya à plusieurs artistes une
ébauche de dessin afin qu’elle soit complétée dans le cadre du fanzine
*Cabaret Voltaire*¨ <span class="footnote">*Post Digital Print, la mutation de l’édition depuis 1894*, Alessandro Ludovico, édition B42, traduit en 2016</span>. Dans le numéro qui a suivi, il a réuni ces
dessins pour en faire, ce qu’on nommerait aujourd’hui, un fanzine
participatif. D’autres projets ont suivi cette approche coopérative,
comme l'*Assembling Press*¨, de Richard Kostelanetz, qui réunissait des œuvres littéraires et
artistiques envoyées par les lecteurices ou encore la revue *Arte
Postale!*¨ qui réalisa un fanzine dans lequel chaque page était dédiée à
un jeu inventé par un·e auteurice différent·e. Un aspect est
intéressant dans ce dernier exemple puisque pour jouer à ces jeux, les usager·ères
devaient manipuler la page, la couper, la plier. Un autre type
d’interaction existait, entre cette fois, non pas que le contenu et
l’usager·ère, mais entre l’objet en tant que tel et son ou sa propriétaire.

Je me suis intéressé aux designers contemporains qui proposent des
systèmes et des processus visant à rendre les lecteurices acteurices et autonomes de différentes manières. Parfois seule, parfois dans le
cadre d’un projet avec Julia Veljkovic, nous avons rencontré plusieurs
designers pour comprendre la manière dont iels procèdent, leurs objectifs
et leurs retours sur ces expériences.

En rencontrant le studio Objet-Papier, nous avons pu échanger au sujet de leur
projet *Print-It*¨. Iels proposent un magazine en ligne, téléchargeable
gratuitement sous la forme d’un PDF, afin de l’imprimer et de le
façonner. Ce projet a attiré mon attention pour plusieurs raisons&nbsp;: l'hommage à la culture Internet, l’idée d’autonomie et la place qu’iels
laissent au lecteurices dans le processus de création.

En laissant les lecteurices imprimer et façonner leur propre magazine,
Objet-Papier choisit de les rendre autonomes en les impliquant&nbsp;: «
Quitte à emprunter des routes plus longues et moins évidentes, le
challenge du “&nbsp;faire autrement&nbsp;” et “&nbsp;faire soi-même&nbsp;” est sans cesse
renouvelé&nbsp;». Le studio propose une notice pour que les personnes
puissent relier leur magazine. Cette notice permet aux futur·es
lecteurices n’ayant jamais fait d’édition de pouvoir participer et de «
réussir&nbsp;» à relier leur objet. Néanmoins, rien n'est imposé, d'autres approches de façonnage du magazine sont possibles, même si la présence de la notice prône forcément une reliure précise.
La question du rôle des notices ou des documentations se pose alors
forcément&nbsp;: sont-elles des sources d’autonomie ou au contraire,
réduisent-elles notre autonomie&nbsp;?

Ensuite, pour que les lecteurices aient leur rôle à jouer dans la
création de leurs objets éditoriaux, pour qu’iels deviennent autonomes,
la question de la participation est centrale. Lorsqu'il est pressenti que
le projet sera plus intéressant s’il est contributif, il est essentiel
de comprendre comment le faire vivre. Nous savons que la contribution ne
sera jamais la même pour tous·tes les participant·es, elle sera plus ou
moins active, mais aussi minimes qu’elles soient, les contributions
doivent surtout être visibles. Toutes ces contributions doivent être
mises en avant et au même niveau, pour Thomas Thibault, du collectif BAM.
Il est essentiel de «&nbsp;ne pas uniquement rendre visible le contenu,
mais aussi rendre visible la participation. En fait, comme en démocratie, les gens ne participent pas quand iels se sentent seul·es à
participer&nbsp;». La façon de rendre compte de ces interactions et
contributions est déterminante.

Par exemple, dans le projet *DONC*¨ réalisé par Sarah Garcin, Louise Drulhe
et Raphaël Bastide, cet aspect est bien présent. *DONC* est une édition
qui a permis de restituer le colloque *Arts et Réseaux sociaux* (mai 2018,
Cerisy-la-Salle). Iels ont mis en place un système d’échange par email,
grâce auquel toustes les participant·es du colloque pouvaient discuter.
Toutes les demi-journées, les discussions étaient imprimées, regroupées
dans une salle commune et les participant·es étaient invité·es à annoter
les impressions. À la fin de l’événement, toutes les discussions et
annotations ont été regroupées dans une édition commune. Ce processus a
permis de rendre compte visuellement des participations et échanges provoquées par le projet et donc d'en inciter d'autres.

Un autre exemple&nbsp;: le fonctionnement du B:Lab, l’atelier numérique de
l’École Boulle, imaginé par le collectif BAM. L’objectif ici était de
proposer aux étudiant·es d’utiliser les machines présentes en
s’auto-formant. Le collectif a alors trouvé des moyens pour donner envie
aux étudiant·es de participer au projet&nbsp;: se former et surtout former
d’autres étudiant·es, une fois qu’iels ont acquis un bon niveau de
connaissance des machines. Des systèmes ont été mis en œuvre pour que les
étudiant·es puissent facilement comprendre qui participe, quelles sont
les compétences de chacun·e et vers qui iels peuvent se tourner pour se
former. Le plus important était de rendre compte visuellement des
participations pour en motiver d’autres. La participation doit être
adaptée au regard des personnes présentes, même si elle ne peut
pas fonctionner parfaitement à chaque occasion, il existe des moyens pour la
rendre possible.

Enfin, la notion de récupération, de détournement, de faire avec
peu de moyens, me paraît être en lien direct avec cette question
d’autonomie. Si l’on souhaite que les personnes puissent participer à la
création de leurs objets, nous devons prendre en compte ce à quoi elles
ont accès. Par exemple, le studio Objet-Papier à construit le magazine
Print-It en prenant en considération le fait que les personnes qui imprimeront
chez elles n’auront surement accès, si elles y ont accès, qu’à une
imprimante A4.

Le programme¨ des événements ayant lieu à Maxima, auquel j’ai participé pendant mon stage chez Luuse, rentre également dans cette thématique de
détournement et d’économie de moyens pour donner de l’autonomie. Ce
programme devait pouvoir être réalisé, sans qu’un·e des membres de
Luuse aient besoin d’apporter son aide à chaque publication mensuelle. Pour cela, c’est la
question du logiciel et des outils utilisés qui était la plus
importante. Trouver un logiciel de mise en page connu par toutes les
personnes du lieu aurait pu s’avérer compliqué. Nous avons donc choisi
de mettre en place un système à partir de tableurs, efficient sur
n’importe quel logiciel de calcul. Les personnes peuvent remplir les
informations des divers évènements sur un tableur partagé. Il leur
suffit d’enregistrer les modifications pour que le site web du programme
se mette à jour. Ce programme est autonome dans le sens ou le système a
été mis en place par Luuse sans avoir besoin d'y intervenir les mois suivants. De la même manière, les utilisateurices de ce
programme sont complètement autonomes puisqu’iels doivent simplement
entrer leurs informations dans le tableur. Quant à l’impression, le
studio Frau Steiner dispose d’un <u>risographe</u> et travaille
également à Maxima. Le programme pourra alors être imprimé sur place et
puisqu’il est en monochromie, le coût de l’impression n’est pas
excessif.

Il est indéniable que dans les projets que je prend comme exemple ici, le lien
entre objet physique et processus numérique permet l’autonomie du projet. De
plus, être complètement autonome me parait impossible, il s’agit de
penser et créer à plusieurs pour obtenir ces projets autonomes. C’est
très souvent une mise en commun des savoir-faire et des connaissances
qui permet la réalisation de tels projets.

#### Les communs, autonomie ou co-dépendance choisie

Il existe en France et Belgique une fédération des
récupérathèques qui a pour objectif d’accompagner vers un usage raisonné
des matériaux grâce au commun, au partage, en école d’art. Cette
organisation permet donc plusieurs actions, qui me paraissent
essentielles dans la création<span class="footnote">http://federation.recuperatheque.org</span>.

D’abord la Fédération se donne pour mission de créer un réseau entre
écoles d’art ou plus largement entre créateurices dans le but de
pouvoir partager des expériences. Comme nous l’avons compris
précédemment, la notion de réseau et de communautés sont importantes pour
atteindre les objectifs suivants&nbsp;: l’autonomie et la radicalité
pour un design plus juste. Il s’agit de favoriser l’échange de matériaux
grâce à leur propre monnaie&nbsp;: la monnaie de la Récupérathèque. Pour
acquérir cette monnaie les étudiant·es doivent récolter des matériaux
qu’iels donneront à la Fédération. Cette manière de faire permet de ne
pas construire un réseau sur un aspect financier, mais sur quelque chose
de bien plus solide&nbsp;: l’égalité et le lien social. Cette monnaie
d’échange locale permet également de donner une valeur différente à des
objets désuets, ils ne sont plus des déchets dénués de valeur, ils
deviennent une ressource utile ou même précieuse.

Il s’agit, dans un deuxième temps, d’inviter les créateurices à lutter
contre le gaspillage de matériaux en favorisant l’utilisation de
matériaux issus de la récupération. La Récupérathèque revalorise les
déchets et les propose à l’échange, par la mise en commun de
ressources. La plupart des matériaux viennent d’anciens projets
d’étudiant·es, d’entreprises, d’institutions culturelles, etc. Ce
fonctionnement permet également aux créateurices de réduire le coût de
leur projet. Le réseau s’étend au-delà des étudiant·es des écoles,
puisqu’il est possible d’adhérer à la Récupérathèque en étant ancien·es étudiant·es ou en tant qu’ami·es et voisin·es de la communauté de l’école.

Ce système de Récupérathèque crée bel et bien un fonctionnement en
autonomie. Mais une autonomie en co-dépendance. En usant de ce
programme, les créateurices deviennent autonomes économiquement,
iels n’ont plus besoin de compter uniquement sur de grandes chaînes de magasin pour
acquérir leurs matériaux. Or iels restent dépendant·es des autres
étudiant·es présent·es dans ce réseau&nbsp;: c’est une dépendance qui est
choisie, ce qui donne naissance à un «&nbsp;commun&nbsp;».

Le *Whole Earth Catalog*, que nous avons évoqué précédement, a été un des
outils de mise en commun avant que le numérique n’apparaisse dans nos
environnements. Plus tard, Stewart Brand et Larry Brilliant créent un
nouveau média, hérité du Whole Earth Catalog&nbsp;: le WELL ou Whole Earth
’Lectronic Link. Il s’agissait ici, d’un réseau qui a favorisé une
certaine autonomie en termes de partage d’informations et de
savoir-faire. Ancêtre du forum, le WELL a été un des premiers réseau où
l’on pouvait rejoindre des conversations virtuelles en se connectant à
un modem. «&nbsp;La place des femmes dans la société y est débattue aussi
bien que celle des hackers<span class="footnote">*Internet Année Zéro*, Jonathan Bourguignon, Édition Divergences, 2021, page 60</span>&nbsp;». Puisque le WELL était un réseau, où
chacun·e pouvait participer, il a été une des premières plateforme en
ligne où un «&nbsp;commun&nbsp;» virtuel était possible.

Dans la revue multitude N°66<span class="footnote">Revue Multitudes, «&nbsp;Ceci n’est pas un programme&nbsp;», n°66, paru en 2017, (consulté le 26 août 2021), [en ligne]</span> la notion générale de «&nbsp;commun&nbsp;» est résumée en trois points. C'est d'abord une ressource en partage, c’est-à-dire un moyen non-marchand de répondre aux besoins des citoyen·nes. Cette ressource est gérée par une communauté (c'est-à-dire un ensemble de citoyen·nes qui partagent leurs
ressources et définissent elleux-mêmes des règles, de l’accès et de
l’utilisation qui en est fait. Néanmoins ces communautés ne
doivent pas être «&nbsp;homogènes&nbsp;» dans leur caractéristique
culturelle et matérielle). Et enfin, il existe parfois une gouvernance de ce dispositif.

Lorsque l’on parle de «&nbsp;communs&nbsp;», d’autonomie ou de co-dépendance choisie,
les récupérathèques semblent être un système qui répond assez bien à
ces préoccupations.

Un «&nbsp;commun&nbsp;» peut-être matériel, l’océan pour les pêcheurs, comme
immatériel, l’encyclopédie Wikipédia. On peut lire dans l’article
*Donner un statut juridique aux communs* que cette notion incarne «&nbsp;la
possibilité d’une alternative à l’ineptie du tout-marché et au cachot
administratif du tout-État<span class="footnote">Revue Multitudes, «&nbsp;Donner un statut juridique aux communs&nbsp;», p.53 à 55, n°66, paru en 2017, (consulté le 26 août 2021), [en ligne]</span>&nbsp;». Les «&nbsp;communs&nbsp;» sont des espaces dans
lesquels il est possible, en tant que citoyen·ne, de créer en réaction
aux dérives du marché et de l’État. Il s’agit alors d’expérimenter en
ce sens, d’être critique et radical·e dans cette démarche, afin
d’échapper aux systèmes dominants qui ne reconnaissent que trop peu les
enjeux sociétaux ou environnementaux. Évidemment les «&nbsp;communs&nbsp;»
doivent apporter un changement radical dans la perception, la
valorisation des activités humaines et doivent ouvrir les yeux sur ce qui est
juste ou non&nbsp;: «&nbsp;C’est la relation, le fait d’être en relation, qui crée
la responsabilité<span class="footnote">Revue Multitudes, «&nbsp;Faire attention les uns aux autres&nbsp;», p.45 à 47, n°66, paru en 2017, (consulté le 26 août 2021), [en ligne]</span>&nbsp;».

Dans le livre *Aesthetics of the Commons*<span class="footnote">Aesthetics of the Commons, Cornelia Sollfrank, Felix Stalder, Susha Niederberger (eds.), édition Diaphanes</span>, les auteurices tentent
de démontrer que l’art peut jouer un rôle important dans l’imagination et
la production d’une réalité bien différente de l’hégémonie actuelle. La
sphère de la culture est elle aussi dominée par le marché économique
entre propriété privée, achat et vente, concurrence, etc. Pour elleux il
n’y a aucun autre moyen que de nouveaux imaginaires, sociaux et
technologiques, pour sortir du système capitaliste. Les imaginaires ne
sont pas seulement des idées ou fantaisies irréalisables, au contraire
elles peuvent être des hypothèses collectives qui influencent les
actions individuelles et collectives. Les auteurices se posent alors la
question suivante&nbsp;: «&nbsp;Comment repenser le monde et nous repenser,
nous-même, en tant que collectivité, et non plus en tant que propriété
individuelle&nbsp;?<span class="footnote">*Ibid.*</span>&nbsp;». C’est l’art qui, d’après elleux, le permettrait. L’art et la culture peuvent donc
avoir une relation étroite avec la notion des «&nbsp;communs&nbsp;» dans le sens
où ils peuvent être des outils de réflexion. Pour Michael Hardt<span class="footnote">Michael Hardt, né en 1960, est un critique littéraire et théoricien politique américain. Il a écrit *Commonwealth* avec Antonio Negri</span> et
Antonio Negri<span class="footnote">Antonio Negri, né en 1933 est un philosophe et homme politique italien</span>, la notion des «&nbsp;communs&nbsp;» est encore bien plus
large. Ils considèrent les connaissances, les langages, les codes, les
informations, les affects comme des «&nbsp;communs&nbsp;». Envisagés de la sorte, les «
communs&nbsp;» seraient des réalités primaires à nos société alors que la production
capitaliste deviendrait secondaire.

Pour ces auteurs, le premier sens du «&nbsp;commun&nbsp;» serait donc en lien avec la
nature. C’est la nature comme richesse commune&nbsp;: air, lumière, terre
fruit, etc. Son deuxième sens serait de l’ordre des résultats obtenus
par la créativité et le travail de l’humain. Ils ajoutent que ces deux points ont progressivement été privatisés par
le capitalisme dans le but d’en tirer profit. Lorsque Pierre Joseph Proudhon,
dans *Qu’est-ce que la propriété&nbsp;?*, écrit en 1840, que «&nbsp;la
propriété c’est le vol&nbsp;»<u class="com">Note de bas de page, d'où vient cette citation</u>, il parlait bien de l’appropriation du «&nbsp;commun&nbsp;»
naturel et du produit du travail ou de la pensée. Enfin, les auteurs
ajoutent un troisième sens à leur définition du «&nbsp;commun&nbsp;»&nbsp;: celui du
droit à la différence, de la possible coopération des singularités, en
opposition à l’uniformisation engendré par l’universalisme en lien avec
la domination du capital et de l’État<span class="footnote">Antonio Negri et Michael Hardt, les mécanos de la Sociale, Pierre Bance, 2013 (consulté le 7 septembre 2021), [en ligne]</span>. Pour résumer, ils font dans
leur dernier livre *Commonwealth* la critique de la propriété
intellectuelle qui briderait l’expansion du «&nbsp;commun&nbsp;».

Il me semble donc important, que la radicalité en design nécéssite de penser «&nbsp;commun&nbsp;» et de créer du «&nbsp;commun&nbsp;».
Comme nous avons pu le comprendre, les pratiques artistiques sont des
espaces propices à l’imagination de manières plus justes d’appréhender
le monde. Pour se faire, et pour que nos projets soient réalisables, la
notion de «&nbsp;commun&nbsp;» devient essentielle. Nous devrions lutter ensemble pour
que nos espaces naturels communs le restent. Nous devrions continuer à
créer ensemble, en co-dépendance choisie, pour que nos productions
fassent sens. Et enfin, nous devons impérativement imaginer, produire,
créer en dehors des normes et non plus être bridé·es par les systèmes
dominants.

Faire du «&nbsp;commun&nbsp;», être autonome et radical·e à la fois sont des
aspects qui me semblent être liés les uns aux autres. Cette
manière d’être et de penser ne pourra produire que des réponses
alternatives, nous permettant de réagir face aux systèmes, économiques,
environnementaux, sociétaux, qui ne sont aujourd’hui plus acceptables.

## II. Création et diffusion alternatives

<br>

<b>«&nbsp;Cette fabrique des outils par les designers marque, selon moi, une rupture avec la logique performative des sciences de l'ingénieur. \…] Dans le cas des machines-outils “maison”, il n’est pas tellement question de production au sens où l'industrie capitaliste peut la pratiquer à grande échelle, mais plutôt de “fabrique” au sens où les designers inventent leurs outils, les modifient, les détournent, les transforment et les combinent. La finalité de leur bricolage n'est pas quantitative, il est davantage question de produire autrement, d'imaginer une production qui s'affranchisse des systèmes de production imposés. Ces manifestations témoignent de la recherche de fabriques alternatives \…]<span class="footnote">*Objectiver*, ouvrage collectif dirigée par David-Olivier Lartigaud, Entretien avec Sophie Fétro, p.123, 2017</span>&nbsp;»</b>

### 1. Détournement, réappropriation et bricolage

#### Création de nos propres outils

En design graphique, la question des outils est centrale. Pour faire
lien avec les notions d’autonomie, de radicalité et de «&nbsp;commun&nbsp;» abordées
précédemment, il me semble en effet important de repenser notre relation
aux outils de création. Les outils, qu’ils soient
numériques ou bien physiques, et l’utilisation, ou non, que nous
choisissons d’en faire a un impact politique, économique et
environnemental. Les outils sont un paramètre important de l’engagement
que peuvent avoir les designers.

Nous pouvons d’abord nous rendre compte de la dissimulation, de plus en
plus fréquente, des entrailles de nos machines. La plupart d'entre elles sont devenues indéchiffrables. Les conséquences de
cet «&nbsp;art de la dissimulation<span class="footnote">Éloge du carburateur, Essai sur le sens et la valeur du travail, Matthew B. Crawford, Édition Cahier libres, traduit en 2010</span>&nbsp;» qu'évoque Mattew B. Crawford sont la passivité et la dépendance
des usager·ères, qui deviennent exclusivement des consommateurices. Dans son
texte *Éloge du carburateur*, il poursuit&nbsp;: «&nbsp;nous avons de
moins en moins d’occasions de vivre ces moments de ferveur créative où
nous saisissons des objets matériels et les faisons nôtres, qu’il
s’agisse de les fabriquer ou de les réparer<span class="footnote">*Ibid.*</span>&nbsp;». Pour cet auteur, le
goût pour l’artisanat, qui revient de plus en plus dans notre société,
est la conséquence d’un manque de relation à nos objets. Nous avons bien souvent
perdu la main sur eux et cherchons alors à nouveau l’autonomie. Les
usager·ères ont besoin de se sentir «&nbsp;responsables<span class="footnote">*Ibid.*</span>&nbsp;». Le savoir-faire
artisanal est cette capacité à «&nbsp;consacrer beaucoup de temps à une tâche
spécifique et de s’y impliquer profondément&nbsp;». C’est un engagement
radical dans une tâche, un engagement avec le monde qui nous entoure.
Justement, l’auteur met en contradiction «&nbsp;engagement actif&nbsp;» et «
consommation discrète&nbsp;». En design graphique, la «&nbsp;consommation discrète
» est très présente. L’utilisation d’outils comme les logiciels
propriétaires<u class="com">Note de bas de page : définition</u> en sont un bon exemple&nbsp;: nous n’avons pas vraiment la main
sur l'éditeur du logiciel, il nous propose des outils, mais nous ne pouvons pas
créer les nôtres, ceux qui répondraient à nos attentes ou nous
permettraient d’être surpris par les résultats. C'est au début des années 1980 qu’un changement radical s’opère&nbsp;: «&nbsp;le droit d’auteur est étendu au code<span class="footnote">*Internet Année Zéro*, Jonathan Bourguignon, Édition Divergences, 2021, p. 68</span>&nbsp;». Les logiciels ne sont plus modifiables, c’est le début des logiciels propriétaires. Ces logiciels donnent l’impression d’avoir une certaine autonomie  puisque nous pouvons créer un grand nombre de résultats. Mais il s’agit en fait d’une
autonomie très limitée, car ce sont les auteurices de ces logiciels qui décident de ce que nous pouvons faire en nous proposant ces outils sans
nous laisser la possibilité d’en ajouter ou de les modifier. Alors pour un engagement actif
et radical envers nos outils, ne devrions-nous pas les créer nous-même&nbsp;?
Avoir le contrôle sur nos outils nous permettrait d’être plus autonome
et d'assumer une plus grande responsabilité. Jonathan Puckey<span class="footnote">Il est le créateur d’outils comme le Text pencil et du dictionnaire en ligne du Stedelijk Museum Bureau Amsterdam</span> écrit
&nbsp;: «&nbsp;Au lieu d’accepter communément les outils uniformisés tous
identiques que nous vendent les grands distributeurs de logiciels, il
nous faut retrouver la relation personnelle que nous avions avec nos
outils. Il nous faut réintroduire des points de friction intéressants
dans nos logiciels très optimisés. Nous devons apprendre à créer des
outils nous-mêmes. Après tout, c’est ça l’ordinateur&nbsp;: un outil de
création d’outils<span class="footnote">Outil (ou le designer face à la production), Azimut n°47, (consulté le 16 septembre 2021), [en ligne]</span>&nbsp;».

En abordant la notion des outils lors de mon entretien avec les
membres de Luuse, ils expliquent qu’ils préfèrent «&nbsp;bricoler&nbsp;» un
outil qui répondra précisément à une demande plutôt que d’utiliser des
outils génériques. Par exemple, le projet
*Poisson Evêque*¨ était un projet pour lequel ils devaient faire un état
des lieux d’un festival, uniquement pendant le temps de cet évènement,
c’est-à-dire, sur quelques jours. Puisqu’il s’agissait de rentrer,
modifier et mettre en forme du contenu rapidement, ils ont choisi
d’utiliser un outil d'écriture collective type <u>Pad</u>. Pour concrétiser ce projet, ils ont aussi utilisé un outil
libre nommé *Imprimons-internet*, qui permet de remplir, à plusieurs si
nécessaire, le texte via ce pad et voir le rendu en temps réel. Il
permet également, comme son nom l’indique, d’imprimer le résultat.

Créer ses outils, c’est aussi refuser ce que les systèmes des éditeurs dominants,
comme Adobe, proposent pour des raisons économiques, sociales ou autres.
Il s’agit alors de créer une «&nbsp;boîte à outil&nbsp;» fabriquée par nous-même
ou utiliser et modifier des outils
créés par d’autres en fonction de nos besoins. Le logiciel libre permet cette ré-appropriation. Mouvement né en 1983 avec le projet GNU (*GNU’s Not UNIX*, littéralement Gnu n'est pas UNIX) «&nbsp;qui vise à créer un système d’exploitation libre<span class="footnote">*Internet Année Zéro*, Jonathan Bourguignon, Édition Divergences, 2021, p. 68</span>&nbsp;», puisque les <u>codes sources</u> sont accessibles et modifiables.

La *PJ Machine*¨ développée par Sarah Garcin, est un outil de mis en page qu'elle a construit en 2016. Ce projet est un bon exemple de ce que pourrait être un des outils d'un·e designer graphique autonome. La *PJ Machine* est «&nbsp;composée de boutons d’arcade colorés. Elle contrôle une interface, développée spécialement pour celle-ci et permet la mise en page collaborative (ou non) de documents imprimés<span class="footnote">https://sarahgarcin.com/projets/pj-machine</span>&nbsp;». Sarah Garcin a été invitée par le groupe de recherche *Algolit* pour utiliser cette machine pendant une semaine de résidence au musée d'Histoire Naturelle de Berne pour le projet *Frankenstein Revisited*¨ en 2016. L'objectif était de faire une édition qui raconte l'histoire de Frankenstein grâce à des chatbots. Pendant notre entretien avec Julia Veljkovic, elle nous explique le déroulé du projet : «&nbsp;On était six, chacun·e écrivait dans son chatbot et l'enregistrait sur un dossier partagé local, puis la machine récupérait tous les textes et tout le monde pouvait faire soi-même sa double page&nbsp;». La *PJ Machine** a permis de travailler différement cette édition, d'une manière qui n'aurait pas été possible avec un logiciel comme Indesign. <del class="com>De plus, toutes les étapes de ce projet ont été complètement autonomes des choix de Sarah Garcin et des membres du groupe Algolit</del><u class="com">Les choix de Sarah ont définis les formes, ce n'est pas un projet qui fonctionne totalement en autonomie</u>. C'est là que nous pouvons comprendre tout l'intérêt de développer nos propres outils. Elle nous dit aussi : «&nbsp;Je pense que la plupart du temps les gens doivent utiliser du code pour concevoir de nouveaux outils. Ça pose plein de contraintes, ça te met dans des zones d'inconfort&nbsp;». Mais c'est bien cette zone d'inconfort qui permet de créer des formes alternatives.

Pour les membres de Luuse, la création de leurs propres outils permet
aussi de produire des résultats innatendus.
Contrairement à beaucoup d’autres qui ne provoquent aucune erreur ou surprises, bricoler ses propres outils permet d’obtenir des
résultats différents, souvent surprenants. Étienne Ozeray dira pendant notre
entretien&nbsp;: «&nbsp;Puis ça nous plaît de toujours remettre en question notre
outillage et leur compréhension. Quoi de mieux pour comprendre un outil
que de le faire soi-même, ou au moins de se le réapproprier pour
en avoir un usage actif&nbsp;». Ainsi, créer ses outils permet
d’être plus engagé·e, aussi bien physiquement qu'intellectuellement, dans sa
pratique de designer. Lorsque Léonard Mabille, du même collectif, me parle de «&nbsp;boîte à
outil&nbsp;» et de «&nbsp;bricolage&nbsp;», il me semble important d’approfondir ces notions.

Le mot «&nbsp;bricolage&nbsp;» a plusieurs définitions.
L'une est le «&nbsp;fait de se livrer à des travaux manuels
accomplis chez soi comme distraction ou par économie&nbsp;» l'autre est un
«&nbsp;travail d’amateur intermittent et d’une technicité sans garantie&nbsp;»<span class="footnote">Bricolage, CNRTL (consulté le 14 octobre 2021), [en ligne]</span>. De
plus l’expression «&nbsp;donner la bricole à quelqu’un&nbsp;» qui signifie «
tromper quelqu’un&nbsp;» lui donne aussi le sens de la «&nbsp;ruse&nbsp;». Le terme «
bricolage&nbsp;» est une dérive de «&nbsp;bricoler&nbsp;» dont «&nbsp;L’idée dominante est
celle de mouvement de va-et-vient&nbsp;» mais aussi «&nbsp;arranger, réparer,
fabriquer en amateur&nbsp;» ou encore «&nbsp;manœuvrer par des moyens détournés
». Enfin, «&nbsp;bricoler&nbsp;» vient de «&nbsp;bricole&nbsp;», qui d’après son étymologie
concerne d’abord des dispositifs militaires de destructions<span class="footnote">Bricole, CNRTL (consulté le 14 octobre 2021), [en ligne]</span>.

Le bricolage a donc plusieurs sens, nous en retiendrons certains pour
l’appliquer au design graphique. Bricoler en design graphique
pourrait être le fait d’effectuer des «&nbsp;mouvements de va-et-vient&nbsp;», qui
s’apparenteraient à de la recherche, de l'expérimentation, pour déconstruire des objets, des
idées, afin de les réparer, les détourner pour se les réapproprier. Le
bricolage serait une rupture avec l’ordre établi, une façon de faire
autrement avec ce que l’on a. Pour Sophie Fétro, designer d'espace, enseignante et docteure, dans l’article
*Bricolage en design*<span class="footnote">Sophie Fétro, «&nbsp;Bricolages en design&nbsp;», Techniques & Culture, 64 | 2015, (consulté le 14 octobre 2021), [en ligne]</span>, il est important de s’émanciper des logiciels de
PAO déterminés pour échapper à des normes et logiques commerciales
propriétaires. Elle considère que le bricolage est&nbsp;: «&nbsp;un rapport alternatif
aux usages dominants qui s’imposent aujourd’hui<span class="footnote">*Ibid.*</span>&nbsp;».

<u class="com">Il y a deux article que je pense évident à citer : « La vie n'est pas une creative suite » et « Adobe, le créatif au pouvoir ». Ils apparaissent dans ta bibliographie met pas dans ton texte</u>
L’autrice fait un autre lien entre la définition du mot «&nbsp;bricolage&nbsp;»
et le rôle qu’il peut avoir en design&nbsp;: «&nbsp;ne relevant en rien de la
tromperie bien que procédant par ruse et ricochets, ce qui consiste à
privilégier un rapport ouvert à la technique et pour lequel tout n’est
pas prédéterminée&nbsp;». Ce point de vue nous rappelle l’effet de surprise que
provoque l’utilisation de nos propres outils évoqué
précédemment. À ce propos, lorsque Claude Lévi-Strauss, parle de la
manière dont procède «&nbsp;le bricoleur&nbsp;» dans *La pensée Sauvage*, il écrit
que son projet sera «&nbsp;inévitablement décalé par rapport à l’intention
initiale, effet que les surréalistes ont nommé avec bonheur hasard
objectif<span class="footnote"> Claude Lévi-Strauss, *La pensée sauvage*, 1962, p. 32, (consulté le 8 juin 2021), [en ligne]</span>&nbsp;». Il précise aussi que la «&nbsp;poésie du bricolage&nbsp;» vient
du fait que le bricoleur «&nbsp;ne se borne pas à accomplir ou exécuter ; il parle<span class="footnote">*Ibid.*</span>&nbsp;» avec les choses.

Le bricolage en design graphique pourrait être une façon de penser ses
outils et ses créations de manière radicale, en rejet de systèmes qui ne
nous conviennent pas. Souvent la technique est partout bien qu’elle
reste dans l’ombre, si l’on use du bricolage il s’agira de mettre la
technique (et les outils) au premier plan. C’est le processus de
création, les choix techniques, le rôle des outils et des gestes manuels
qui seront mis à l’honneur, pour une critique de l’état actuel des
outils (chers et largement limités) que l’on propose aux designers
graphiques. Le bricolage est une forme alternative de création, dans le
sens où les résultats seront souvent inattendus, et dont les notions de
détournement, de réappropriation et de «&nbsp;communs&nbsp;» sont essentielles.

Le projet de Axel Morales *Piezzographie*¨ est un bon exemple de «
bricolage&nbsp;» en design graphique qui permet l’apparition de résultats inattendus.
*Piezzographie* est une imprimante jet d’encre composée «&nbsp;d’une tête
d’impression mobile, un mélangeur d’encre et un logiciel de traitement
de l’image par système de points<span class="footnote">Étapes n° 222&nbsp;: Design graphique & Culture visuelle, 2014, p. 162</span>&nbsp;». En prenant l’objet dans
sa main et en le passant au-dessus d’une feuille, les usager·ères
pourront imprimer leurs images. Il y a ici une rencontre entre processus
numérique et geste manuel très intéressante. Le système d’impression
laser classique a été détourné pour proposer une manière alternative
d’imprimer.

#### Détourner les canaux et supports de diffusion habituels, pourquoi&nbsp;?

En plus des outils, les supports et canaux de diffusion que nous utilisons ont un rôle important dans notre démarche de design radical. Lorsque l'on réalise un projet, ça n'est pas seulement une question de création. La manière dont nous le publions, les endroits où il est présenté, comme les personnes avec qui nous choisissons de travailler, sont porteur de valeurs.

Les supports de diffusion sont des objets en
constante évolution technique. Pour de nombreuses raisons, ils ont été souvent
modifiés, réappropriés par le peuple. Par exemple, le *dazibao*¨ en
Chine, littéralement «&nbsp;journal à grands caractères&nbsp;» est apparu très
tôt. Cet affichage est un moyen traditionnel utilisé de longue date en
Chine<span class="footnote">Claude Quétel, *Histoire des murs*, page 295 à 306, 2012, (consulté le 18 octobre 2021), [en ligne]</span>. Il s’agissait d’affiches réalisées à la main sur de grands
morceaux de tissu ou de papier et placardés sur les murs par des
citoyen·es mécontent·es du système qui était en place à l’époque. Ces
affichages traitaient le plus souvent de sujets politiques et pouvaient
donc être lus par tout le monde. Il existe une photo de ces *dazibaos*
placardés sur des murs qui date de 1915<span class="footnote">Dazibao, Wikipédia (consulté le 18 octobre 2021), [en ligne]</span>, mais c’est en 1966 avec la
Révolution culturelle lancée par Mao Zedong que les *dazibaos* se sont à
nouveau développés<span class="footnote">*Ibid.*</span>. Ce moyen de communiquer était alternatif dans
le sens où il permettait de ne passer par aucun système qui aurait pu
censurer la parole critique des dissident·es du régime de Mao Zedong. C’était un moyen de diffuser des messages de
manière autonome, peut-être une première tentative en termes
d’auto-publication.

Bien plus tard, les supports et canaux de diffusion ont aussi connu des
formes alternatives à l'époque où le monde de l’auto-édition est apparu.
Comme vu précédemment, les supports de diffusion ont
souvent changé de forme. Ils n’étaient plus seulement d’imposantes
éditions, ils se sont transformés&nbsp;: micro-édition, flyer, fanzine,
journal, etc. Dans les années 60, un mouvement artistique nommé *Fluxus* fondé par George Maciunas, a vu le jour. L’un des premiers objets imaginés
a été la «&nbsp;boîte Fluxus&nbsp;»¨, une boîte remplie de jeux en tout genre,
d’objets en plastique et d’objets imprimés (brochure, tract, cartes,
etc). Cet objet a ouvert le champ des possibles en matière d’édition et
de diffusion. Ensuite, George Maciunas a rassemblé des coupures de
presse relatives à Fluxus sous la forme d’un journal enroulé qu’il ira
ensuite afficher dans les rues. Enfin, c’est toujours les fondateurices
du mouvement *Fluxus* qui ont pour la première fois défini le terme de
réseau, qu’iels appelaient «&nbsp;réseau éternel&nbsp;». Il s’agissait, d’après
elleux, d’un réseau avec «&nbsp;\...un modèle d’activité créative où aucune
frontière ne sépare l’artiste du public, l’un et l’autre œuvrant
ensemble à une création commune<span class="footnote">*Post Digital Print, la mutation de l’édition depuis 1894*, Alessandro Ludovico, édition B42, traduit en 2016, p.42</span>&nbsp;». *Fluxus* a eu une grande influence
sur l’auto-édition, Phyllis Johnson qui dirigeait la revue expérimentale
*Aspen*¨, et qui s’était beaucoup inspiré des objets de *Fluxus* dira&nbsp;: «&nbsp;Nous voulions en finir avec le format obligé, vraiment ultra
contraignant, du magazine<span class="footnote"> *Ibid.* p.44</span>&nbsp;». Et encore aujourd’hui ce type d’objets composite
est parfois utilisé et revisité par des designers.

La photocopie a également permis de créer des supports alternatifs assez
facilement. De plus en plus de fanzines de très grand format, parfois
directement affichés sur les murs, sont apparus. Concomitamment à
l’explosion de l’auto-édition punk (possible surtout grâce à la
photocopie), le *Mail Art* se mit lui aussi à produire des objets
imprimés. Mais cette fois davantage dans l’objectif de repenser le moyen de
diffusion et de communication. Le *Mail Art* utilisait les services de la
poste pour établir la communication entre des artistes parfois séparé·es
par des frontières, détournant les politiques strictes de certains pays. La lettre et
l’enveloppe devenaient des supports d’expression. L’<u>héliographie</u>, technique d’impression apparue en 1822, a également été utilisée à cette époque pour
réaliser des fanzines. Des étudiant·es en architecture ont imaginé une
nouvelle forme et une nouvelle façon de diffuser des fanzines.
L’héliographie permettait d’imprimer sur des longues feuilles de papier
enroulées. Afin de distribuer ces fanzines les exemplaires étaient «&nbsp;transmis directement sur les machines des lecteurs, le plus souvent
selon un schéma pyramidal de distribution...<span class="footnote">*Ibid.* p.52</span>&nbsp;». C’était plus ou
moins des «&nbsp;fax zines&nbsp;» envoyés grâce à des télécopieurs.

À partir des années 80, au début de la révolution numérique, beaucoup
avaient accès aux ordinateurs. Il était alors possible de se
procurer les fanzines par le biais de disquettes. Des revues
interactives sont apparues, rassemblant disquettes, CD et objets
imprimés. Par exemple, la revue *Adenoidi*¨ proposait à ces lecteurs de
retrouver sur une disquette, qui accompagnait son fanzine imprimé, des
images en couleur. Toutes ces images étaient présentes dans l’objet
imprimé mais en noir et blanc. C’est la revue *Blender*¨ qui a été l’une
des premières à imaginer son fanzine à travers un CD-ROM. Le fanzine, en
plus des textes, contenait de la musique ainsi que des publicités.

Plus récemment, un des mouvements emblématiques de la création de son
propre support a été celui des *Cartoneras*¨<span class="footnote">Mouvement fondé par Washington Cucurto, Javier Barilaro et Fernanda Laguna</span>. Cette alternative
éditoriale à contre-courant est un mouvement d’édition sociale,
politique et artistique qui a débuté en Argentine en 2003<span class="footnote">Cartonera, Wikipédia (consulté le 18 octobre 2021), [en ligne]</span>. Cette
initiative a pris vie en réaction à la crise économique de 2001 au cours
de laquelle le *pesos* a fortement chuté. Ces conditions économiques
difficiles ont fait augmenter le nombre de *Cartoneros*&nbsp;: des personnes
qui gagnent leur vie en collectant et vendant des matériaux récupérés
dans les usines de recyclage. Les livres *Cartoneras* sont fabriqués à
partir de matériaux récupérés, principalement du carton, achetés aux
*Cartoneros.* Toutes les éditions, de la couverture aux pages
intérieures, sont entièrement réalisées grâce à ce carton. Les
couvertures étaient généralement peinte à la main et vendues dans la rue
dans le but de favoriser l’accès à la littérature. Aujourd’hui, ces
éditions sont aussi souvent disponibles via leurs pages Web. Le
mouvement des *Cartoneras* est autonome autant dans la production de ses
supports de diffusion que dans la diffusion elle-même.

Aujourd'hui encore, il existe plusieurs projets pour lesquels leurs auteurices
pensent les supports d’une manière alternative, en réaction aux
problématiques économiques ou écologiques. Le projet *La Perruque*¨ mené
par Olivier Bertrand est un exemple de création alternative. La perruque
est une revue de 1 x 90 centimètre de long, imprimée sur les marges
d’impression offset et chaque numéro de la revue est un spécimen d’un
caractère typographique. Ce projet «&nbsp;détourne les surfaces de papier
inutilisées de la production en imprimerie, de sorte qu’elles deviennent
carte blanche pour typographes au lieu d’être des déchets<span class="footnote">«&nbsp;La Perruque hacks unused paper surfaces from print shop production, so they become carte blanche for type designers instead of waste&nbsp;», http://la-perruque.org</span>&nbsp;».
Olivier Bertrand a débuté ce projet lorsqu’il était étudiant en
collaboration avec l’imprimerie Media Graphic à Rennes. C’était un
moyen d’imprimer ses publications sans avoir à dépenser d’argent pour le
papier et l’impression. Par la suite, Olivier Bertrand a continué ses
expérimentations «&nbsp;anti gaspillage&nbsp;». Il dit&nbsp;: «&nbsp;je pirate
essentiellement les surfaces de papier inutilisées de la production de
l’imprimerie&nbsp;». De cette manière il a pu agir contre le manque de moyens
économiques que peuvent rencontrer les designers au moment de leurs
études tout en répondant à la volonté d’éco-conception.

L’Atelier Téméraire, fondé par Marion Bonjour, donne aussi une réponse
assez radicale à cette question de diffusion alternative face aux
contraintes économiques par l'auto édition. Les coûts de
fabrication de ces éditions sont généralement peu élevés puisqu’elles sont réalisées
artisanalement&nbsp;: imprimantes lasers récupérées ou reconditionnées,
impression en noir et blanc, souvent sur des macules de papier. Marion
Bonjour m'explique pendant notre entretien&nbsp;: «&nbsp;Il faut essayer d’être un peu
inventif, si tu n’as pas l’argent pour acheter le papier, comment tu
peux faire&nbsp;?&nbsp;». Par exemple, la réédition du mémoire *Des cycles et des
hommes* d’Angélique Boudeau a été produit à 30 exemplaires composés
chacun de 70 pages&nbsp;: tous les papiers utilisés dans cette édition sont
issus de la récupération. Elle ajoutera, lorsque je lui pose la question
du résultat du workshop Faire Signe organisé par Le Signe à Chaumont, que le format
papier a très peu été utilisé. Et cet accès limité au papier apporte souvent
des formes surprenantes, qu’il me semble important de prendre en compte
dans le questionnement de la diffusion alternative. Justement, lorsque
l’on évoque les systèmes de diffusion alternatifs elle précise&nbsp;: «&nbsp;Même si
on ne peut pas révolutionner tout de suite les méthodes de diffusion, on
peut réfléchir à d’autres manières de faire. Imaginons qu’on ne puisse
plus faire de communication papier, comment faire pour garder un rapport
à l’objet&nbsp;?&nbsp;». Les productions de l’Atelier Téméraire me font penser à
celles des Cartoneras&nbsp;: avec peu de moyens iels arrivent à diffuser des
informations, des textes, de la littérature, chose qui n’aurait pas été
possible sans l’utilisation de ces moyens alternatifs.

Finalement, nous pouvons nous rendre compte assez vite que les canaux de
diffusion institutionnels sont souvent des freins ou des sources de
contraintes pour celleux qui ont la volonté de faire passer des messages
ou pour celleux qui les utilisent. Ils sont souvent très peu adaptés aux contraintes économiques, sociales, écologiques de notre société. Parce qu’ils ne sont pas accessibles
techniquement ou économiquement, parce qu’ils ne respectent pas une logique d’économie de ressources énergétiques, <u class="com">parce qu’ils ne
respectent pas nos vies privées, etc. (je n'ai pas compris ce que ça venait faire là. Évite de terminer par un « etc »)</u>

Par ailleurs, Pauline Palayer, dans son mémoire *Design graphique par temps
d’orage*<span class="footnote">Pauline Palayer, *Design graphique par temps d’orage*, mémoire DNSEP, ESAD Valence, 2020</span>, retranscrit une phrase d’Olivier Bertrand à propos de la
précarité qui induit le projet&nbsp;: «&nbsp;\[\...\] La constante de Surfaces
Utiles c’est que je n’ai pas un rond, mais de cultiver cette pauvreté
c’est ce qui permet de publier ces trucs qui ont cette singularité<span class="footnote">*Ibid.* p.38</span>&nbsp;». Nous pouvons en effet constater, avec ces projets, que faire
avec peu de moyens et dans une logique d’éco-conception impose souvent de choisir des supports alternatifs de diffusion.

####  Création de nos canaux de diffusion numérique

<u class="com">Il y a des réseaux sociaux, des chats et des forums qui fonctionnent par fédérations qui réunissent beaucoup d'acteurices design/art et outils libre. ex : post.lurk ou encore 80 columns</u>

Les canaux de diffusion numérique ont aussi tout intérêt à être
repensés. Pour aborder cette notion de canaux de diffusion numérique
alternatifs, nous allons prendre comme exemple les <u>bibliothèques
pirates</u>. Ce
genre de projet est la plupart du temps construit en réaction aux
bibliothèques institutionnelles qui présentent souvent un contexte inégalitaire dû à leur inaccessibilité au plus grand nombre, à l’invisibilité de certaines ressources ou encore à cause de leur système de classification.

Les bibliothèques pirates sont crées pour défendre l'accessibilité gratuite à des ressources&nbsp;: «&nbsp;la gratuité est un service que la collectivité se rend à elle-même&nbsp;<span class="footnote">*Du bon usage de la piraterie*, Culture libre, sciences ouvertes, Florent Latrive, La Découverte, 2007, p.102</span>». En opposition à cette idée, en 2000 un groupe d'auteurices et d'éditeurices ont exigé le retrait de leur livre des bibliothèques si leur prêt n'était pas payant<span class="footnote">*Ibid.*</span>. Mais combien n'auraient pas pu se découvrir un attrait pour la lecture, un centre d'intérêt, sans l'accès libre aux livres dans les bibliothèques, dès notre plus jeune âge&nbsp;? Cette bataille de la lecture accessible ne se limite pas aux bibliothèques en tant que lieu physique, mais aussi aux bibliothèques en ligne &nbsp;: les bibliothèques pirates.

<u class="com">Deux textes de Aaron Schwatz me semble important. « Contre-argument : télécharger n'est pas voler ! » et « Manifeste pour une guérilla de l'accés libre »</u>

Avant d'analyser les enjeux d'une bibliothèque pirate, nous allons tenter de comprendre pourquoi nous les appelons comme celà. Les mots «&nbsp;pirate&nbsp;» ou «&nbsp;piratage&nbsp;» sont très controversés. Floriant Latrive écrit «&nbsp;la guerre aux pirates a été déclarées&nbsp;<span class="footnote">*Ibid.* p.14</span>» au nom de la propriété intellectuelle et surtout de l'économie mondiale. Souvent ces discours anti-pirate usent de termes excessifs, généralement associés à la guerre. Beaucoup tentent de faire croire que le piratage est un grave crime, responsable de la chute des ventes et donc de l'économie. Par exemple, à la suite de copies de médicaments anti-sida par des entreprises indiennes, le dirigeant d'un laboratoire pharmaceutique a parlé d'«&nbsp;actes de pirateries qui seront éradiqués comme l'a été la piraterie maritime au XVIIè siècle&nbsp;<span class="footnote">*Ibid.* p.14</span>». Jack Valenti, un des lobbyistes pro-droit d'auteurice, aborde le piratage comme «&nbsp;notre guerre à nous contre le terrorisme&nbsp;<span class="footnote">*Ibid.* p.14</span>», pour d'autres c'est un «&nbsp;crime collectif contre la culture&nbsp;<span class="footnote">*Ibid.* p.20</span>».  

Les pirates, dans ce contexte, tentent de détourner la propriété intellectuelle pour l'échange et la circulation libre de ressources et de savoirs. Iels sont investi·es dans des pratiques d'échanges non-monétaires. Ce terme peut aussi, il me semble, faire écho à celui de *hacker*&nbsp;: les deux défendent le domaine public et ont pour maître mots l'échange et le partage. Nous ne devons pas confrondre les *hackers* et les *crackers*. Florent Latrive explique : «&nbsp;La distinction est d'importance car l'activité du hacker est légale alors que celle du cracker est illégale. En français, on retrouve la même confusion avec la traduction malheureusement courante de hacker et cracker par le même terme de pirate<span class="footnote">*Du bon usage de la piraterie*, Culture libre, sciences ouvertes, Florent Latrive, La Découverte, 2007, p.114</span>&nbsp;».

Or, le partage libre de ressources peut très bien co-exister avec la vente de ces mêmes ressources, sans que l'un n'entâche l'autre. Avoir un livre, comme objet tangible, entre les mains et lire ce même texte sur un écran (ou imprimé en texte brut) ne provoque en rien les mêmes sentiments chez les lecteurices. L'un et l'autre pourraient être compatibles, ils ne seraient seulement pas utilisés dans les mêmes contexte&nbsp;: lecture rapide ou lecture appuyée, intérêt pour l'objet livre ou pas, etc. Il me semble alors que les discours extrêmes sur les dangers du piratage n'ont pas lieu d'être et sont même irréalistes. Ces lois n'empêchent d'ailleurs pas, les personnes qui le souhaitent, de récupérer des savoirs qui sont protégés par la propriété intellectuelle. C'est pourquoi Floriant Latrive plaide dans son livre pour un «&nbsp;bon usage de la piraterie&nbsp;» plutôt que pour une tentative d'éradication qui finalement n'aboutira jamais. N'oublions pas que plus les ressources seront protégées par la propriété intellectuelle, plus la circulation des savoirs sera empêchée. Alors les inégalités ne feront que se creuser.

Dans le cadre de l’occupation temporaire de Maxima, l’envie de créer un
dispositif de réseau local de ressources immatérielles, c'est-à-dire une bibliothèque pirate, est apparue comme un outil d'inclusion sociétal. Ce
dispositif, a pour objectif de favoriser les
échanges de ressources, textuelles pour le moment, entre les personnes
qui occupent ce lieu. Ce réseau constitue la
ressourcerie de Maxima et sera accessible à toutes les personnes du lieu
en se connectant au Wifi Bibliotecha. Chacun·e sera libre de consulter,
téléverser ou de télécharger des documents. Nous nous appuierons sur ce projet pour aborder les différentes étapes de la construction d'une bibliothèque pirate. D'abord, la possible réalisation d’une charte qui prendra en compte les
questions de droits d’auteurices, ensuite la classification des ressources, puis l’interface de la bibliothèque pirate.

Pour commencer, une charte peut permettre d'annoncer clairement ce qu'il est possible, ou pas, de faire avec ces ressources vis-à-vis des droits d’auteurices, de reproduction et de diffusion. Les utilisateurices sont libres de récupérer les documents, néanmoins la charte peut permettre d’expliquer qu’une fois les ressources téléchargées, les personnes deviennent responsables de ces documents, de leur utilisation et de leur éventuelle diffusion. Évidemment, rien n'oblige la création de cette charte, sa présence pourrait simplement être utile pour informer de nos responsabilités en tant qu'utilisateurice de ce réseau.

Ensuite, pour la question de la classification, il s'agira de ne pas adopter les mêmes principes présents dans les bibliothèques institutionnelles. Ces classifications,
puisqu’elles sont rarement modifiées, ont tendance à faire perdurer de
vieux stéréotypes. La *Théorie
Queer*<span class="footnote">*Queering the Catalog&nbsp;: Queer Theory and the Politics of Correction*, Emily Drabinski,
Brooklyn Library Faculty Publications, 2013</span> de classification est intéressante pour cela. Elle propose que la classification ne soit pas fixe,
c’est-à-dire qu’elle ne soit plus décidée par seulement quelques
personnes pour un temps indéfini, et sans pouvoir être remises en
cause par les lecteurices. Il s’agit aussi dans la *Théorie Queer* de
redéfinir les catégories et les dénominations des ressources en prenant
soin d’organiser avec précision et respect les documents sur les groupes
sociaux sous représentés. C’est-à-dire que les
identités dites marginales doivent pouvoir se retrouver dans cette
bibliothèque. Un des principes pouvant être adopté dans ce genre de bibliothèque est de permettre aux lecteurices de
participer entièrement à la classification. Chacun·e est libre
d’ajouter des mots-clés, de modifier la description d’une ressource, de
remettre en cause certaines informations de classification, etc. De ce fait, la classification n'est plus
stable et objective mais elle est une structure en perpétuelle évolution
qui invite les usager·ères à s’engager de façon critique.


Enfin, un autre des points importants concerne la manière dont l’interface est réalisée pour qu’elle ne mette pas en valeur certaines
ressources, personnes ou identités, au détriment d'une autre. Il existe pour cela plusieurs manières de faire, mais il est important que les créateurices de bibliothèque pirate y réfléchissent.


Pour résumer, la documentation partagée de Maxima est un canal de
diffusion libre, alternatif et local. Ici, la diffusion se fait au sein
d’un espace restreint, la base de ressources sera alors, certes plus
limitée que dans une bibliothèque institutionnelle, mais plus intime et
représentative des intérêts de chacun·e, ce qui permettra de créer du lien
entre les personnes. Il s’agit essentiellement d’une remise en question
des systèmes de diffusions institutionnels.

L’utilisation dans ce projet d’un *Raspberry Pi*¨, un nano-ordinateur, a aussi son importance. Le Raspberry Pi
a été créé dans un premier temps pour démocratiser l’accès aux
ordinateurs&nbsp;: très abordable (entre 25 et 40 euros) il permet
l’utilisation de logiciels libres. De plus, il est possible de se
procurer un *Raspberry Pi* nu, sans boîtier, câble, clavier ni écran ce qui
permet d’utiliser du matériel provenant de récupération. Ces
nano-ordinateurs sont dotés d’une puce Wifi ce qui permet aux
utilisateurices de se connecter au réseau Wifi du *Raspberry Pi*, qui est
uniquement dédié au fonctionnement de ce projet pour l’échange de
ressources.

À Bruxelles, l'École de Recherche Graphique (ERG) a réalisé un projet similaire nommé *Rideau de perle*. Iels ont imaginé une bibliothèque qui réunit des livres «&nbsp;queer, rares, volés, scannées, transformés en pdf<span class="footnote">https://wiki.erg.be/w/Rideau_de_perles</span>&nbsp;». Pour accéder à ces ressources il suffit de se connecter au réseau wifi de l'école (iels précisent que celui-ci émet jusque sur le trottoir de l'ERG) et de se créer un compte. En plus de télécharger les livres déjà présents, il est possible d'ajouter ces propres textes. Pour poursuivre cette expérience, les étudiant·es ont réalisé un *bookscanner*¨ grâce à des notices de construction disponible sur le web. Ce scanner de livres permet de produire des versions numériques de livres encore non disponibles en version numérique. De la même manière que la bibliothèque de Maxima, ils utilisent un Raspberry Pi.  

Le site *Infokiosques.net*<span class="footnote">https://infokiosques.net/</span> <u class="com">Atelier Téméraire tire son origine en partie de cette pratique</u>, fait aussi partie de ces expérimentations pour la diffusion et l'accès libre aux ressources. Un infokiosque est une librairie «&nbsp;alternative et indépendante&nbsp;» dans laquelle des ressources sont rassemblées puis diffusées librement. Ces lieux sont d'abord des librairies physiques, la plupart du temps misent en place dans des lieux associatifs ou des squats. Originaires d'Allemagne, elles se trouvent maintenant dans de nombreux pays et permettent de regrouper des personnes autour de sujets, le plus souvent, militants. Sur le site *Infokiosques.net*, une notice explicative pour comprendre comment monter son infokiosque est disponible. La gratuité (ou les prix libres) y sont très important puisqu'il s'agit de promouvoir l'accès libre aux savoirs. *Infokiosques.net* existe depuis 2002 et permet de réunir les ressources déjà diffusées sur papier dans les infokiosques physiques pour les diffuser sur le net et les rendre accessibles à un plus grand nombre de lecteurices. Aujourd'hui, il existe en France une trentaine de ces lieux, dont certains sont nomades.

Tous ces exemples de bibliothèques numériques démontrent l'importance d'imaginer des canaux de diffusion libres alternatifs. Florian Cramer dans la postface du livre *Post-Digital Print*, tente de donner une définition d'un imprimé postnumérique. Il écrit&nbsp;: «&nbsp;l’imprimé postnumérique
devrait inclure le partage communautaire en réseau, qui est à la fois
local/tangible et mondial/numérique ; la réunion des deux opposés de la
formation et de l’information<span class="footnote">*Post Digital Print, la mutation de l’édition depuis 1894*, Alessandro Ludovico, édition B42, traduit en 2016</span>&nbsp;». Il donne comme exemple
le projet *Mag.net*, un réseau d’«&nbsp;éditeurs culturels électroniques&nbsp;» mené
par l’auteur de ce livre, Alessandro Ludovico. Ce projet, mené de 2006 à
2008, avait pour but de «&nbsp;combiner ces deux modes de partage à travers
une démarche éditoriale <u>hybride</u> mêlant forme électronique et impression
à la demande<span class="footnote">*Ibid.* p.189</span>&nbsp;». Ce mode de diffusion hybride présente l’opportunité de créer ou d’utiliser des supports de diffusion
alternatifs. Ce livre lui-même en est un bon exemple&nbsp;: il a été imprimé en offset
traditionnel (impression à la demande)<u class="com">Attention print on demande différent de l'imprimerie traditionnelle</u> et a été distribué à travers
différents canaux «&nbsp;underground ou officiel&nbsp;» comme sur AAAAARG.ORG, une bibliothèque électronique pirate, avant sa traduction chez B42.

Il me semble que ce genre de projets, qu'ils soient physiques ou numériques, se doivent de prendre en compte une notion importante&nbsp;: l'éco-conception. D'abord, pour des raisons évidentes de responsabilité face à l'urgence environnementale, mais aussi puisque nous parlons de projets hybrides. En effet, si une même ressource doit être diffusée de manière tangible et numérique, pour rentrer dans la définition de l'imprimé postnumérique dont nous avons parlé plus haut, nous devons faire en sorte qu'elle ne soit pas source de plus de gaspillage. Pour espérer produire des projets hybrides sans avoir plus d'impact sur l'environnement, voire d'en avoir moins qu'un projet qui est diffusé sous une seule forme, il est important de penser à des processus d'éco-conception.  

Il s’agit de créer, bricoler des outils ou des systèmes de diffusion alternatifs et radicaux dans leurs formes et leurs fonctionnements, qui prennent en compte les enjeux environnementaux actuels.

### 2. L'éco-conception

#### De la responsabilité des créateurices

L’éco-conception, appliquée aux objets physiques ou numériques est un
aspect important dans le champ du design graphique radical. Cette notion
intègre la problématique de l’environnement, de la conception d’un
produit, ou d’un service, à toutes les étapes de son cycle de vie.

L’éco-conception, en design graphique, suppose qu'une attention
particulière soit portée par les designers dès le début de la conception d’un
objet. La méthode peut s’appliquer à plusieurs paramètres. Dans la
création d’objet éditoriaux, le principal choix concerne surtout l’utilisation
du papier. En effet, le papier est au centre du travail des designers,
il est important du début jusqu’à la fin du projet&nbsp;: brouillon,
esquisse, maquette, objet final. Pauline Palayer propose dans son
mémoire<span class="footnote">Pauline Pallayer, *Design graphique par temps d’orage*, mémoire DNSEP, ESAD Valence, 2020</span> une manière assez claire de comprendre où peut se situer le
gaspillage dans la création d’un objet graphique, elle dit&nbsp;: «&nbsp;en design
graphique \[l’encombrement\] peut aussi se penser par la contre forme
qu’elle induit&nbsp;: par ce qu’on souhaite soustraire pour donner plus de
vigueur à sa forme<span class="footnote">*Ibid.* p.9</span>&nbsp;». En effet, beaucoup d’objets graphiques
prennent des formes complètement différentes de la coupe de base des
papiers, c’est-à-dire rectangulaire. Les cartes de visite ou encore les
stickers, imprimés à foison (j’y reviendrai plus tard) en sont un bon
exemple. Ces découpes provoquent beaucoup de chutes de papiers qui ne
seront pas réutilisables par les imprimeureuses puisque difformes ou trop
petites. Parfois ce sont les éditions dont la forme ou
l’imposition n’ont pas été réfléchies pour éviter la perte de papier, qui
provoquent beaucoup de gaspillage. Pour résumer, tout ce qui est utilisé
par les designers mais qui ne sert pas à l’objet final est de la matière
«&nbsp;perdue&nbsp;». Pour éviter ces pertes, les designers doivent penser leur
projet avec plus de sobriété et de simplicité. C’est-à-dire réfléchir à
la forme de l’objet en prenant comme paramètre principal le format du
papier et la manière de l’utiliser au mieux.

La notion de chute de papier est donc un aspect important dans
l’éco-conception, il en existe d’autres. Par exemple&nbsp;: le nombre
d’exemplaires d’objets imprimés. Ne faudrait-il pas être plus juste
et prendre le temps de mieux s’informer
quant aux besoins des prochain·es utilisateurices ou lecteurices&nbsp;? Il
s’agirait d’être plus économe dans la création et la production. Être plus
juste, ce serait donc être moins consommateurice, et c’est le rôle de
chacun·e d’entre nous si l’on veut répondre à l’exigence environnementale
qui est aujourd’hui urgente. À ce propos, Pierre-Damien Huyghues
explique pourquoi il vaut mieux utiliser les mots «&nbsp;développement juste
» plutôt que «&nbsp;développement durable&nbsp;»&nbsp;: «&nbsp;Si le développement actuel
est injuste ou si par hypothèses dans notre façon de conduire la
technique il y a de l’injustice, le fait que ça dure n’est pas une bonne
chose<span class="footnote">Emmanuel Tibloux et Pierre-Damien Huyghues, *Design, moeurs et morale*, texte paru dans Azimut n°30, 2008</span>&nbsp;».

Enfin, l’excès de distribution de ces petits objets communément nommés «&nbsp;*goodies*&nbsp;» reflète bien cette surproduction des designers, et donc l’hyper-consommation qui s’en suit, au détriment de l’environnement .L'*Eco-cup* qui reste néanmoins du plastique, le *Tot Bag* la plupart du temps
produit à l’autre bout du monde ou encore stickers, stylos, cartes en
plastiques distribués à tout va. Tous ces objets ne sont pas réalisés
par nécessité, ils ne sont pas utiles. Les designers ont bel et bien
une responsabilité face à la dégradation de l’environnement.<u class="com">Beaucoup des objets que tu as cité précédemment ne sont plus ou n'ont jamais été à la charge des designer. Et c'est surement plus grand que le rôle du designer, c'est une problématique systémique</u> En 1971, Victor J.
Papanek<span class="footnote">Victor J. Papanek est un designer afro-américain (1923-1998)</span> dénoncait déjà la société de gaspillage et défend le design
responsable d’un point de vue écologique et social, il dit&nbsp;: «&nbsp;la
meilleure chose que le designer puisse faire pour l’humanité est
d’arrêter de travailler complètement<span class="footnote">«&nbsp;In an environment that is screwed up visually, physically and chemically, the best and simplest thing that architects, industrial designers, planners, etc., could do for humanity would be to stop working entirely.&nbsp;» Papanek, V. (1971)</span>&nbsp;». Nous pouvons ré-interpréter
cette phrase en suggérant que les designers ne soient pas designers de
tout et de rien. Leur refus face à certains projets «&nbsp;inutiles&nbsp;» est
plus que souhaitable. Pour cet auteur, les designers continuent de
perpétuer de vieux principes principalement fondés sur la logique
économique, souvent inadaptés aux problématiques écologiques.

Revenons rapidement sur la notion de designer responsable, elle me
paraît importante à défendre. Victor J. Papanek a défendu cette notion ainsi que
celle de l’engagement. Il considérait que les designers ont «&nbsp;le pouvoir de
changer, modifier, éliminer ou faire naître de nouveaux modèles<span class="footnote">«&nbsp;How has the profession responded to this situation? Designers wield power over all this, power to change, modify, eliminate, or evolve totally new patterns.&nbsp;» Papanek, V. (1971)</span>&nbsp;».
Pour l’auteur, il y a un vrai problème pour les designers lorsqu’il
s’agit de faire la différence entre répondre aux besoins du marché et
aux besoins des consommateurices. Leur engagement pour ne pas
participer à la société de surconsommation est important. Pour Emmanuel
Tibloux, considérer les enjeux de cette crise politique, sociale
et écologique «&nbsp;a pour effet d’accroître la responsabilité du designer
au regard de l’humanité et conduit à ce titre à ouvrir largement le
champ du design au registre de l’éthique<span class="footnote">Emmanuel Tibloux et Pierre-Damien Huyghues, *Design, moeurs et morale*, texte paru dans Azimut n°30, 2008</span>&nbsp;».

Pour résumer, les designers ont un rôle essentiel à jouer pour la
préservation de l’environnement. De la décision à accepter un projet ou
pas, du choix des matériaux, jusqu’à la diffusion, les designers sont
responsables. Pour Pierre Damien-Huyghues la responsabilité est liée à
l’humain&nbsp;: «&nbsp;Nous sommes responsables de l’être au monde en général en
tant qu’humain. C’est une des propriétés des êtres humains que d’être
voués à la responsabilité, c’est-à-dire à répondre de leur conduite et à
se conduire à partir de questions qu’ils se posent et auxquelles ils
répondent<span class="footnote">*Ibid.*</span>&nbsp;». Refuser radicalement les projets qui ne correspondent
pas à notre morale sociale et écologique peut faire évoluer les mentalités.
Cette notion de responsabilité écologique s’applique aux projets
physiques, mais il est indéniable que cela doit aussi s’appliquer aussi au monde du
design numérique.

#### *Low-tech*, une solution juste ?

L’éco-conception dans la pratique numérique, <del class="com">autrement dit le «&nbsp;<em>low-tech</em>&nbsp;»</del><u class="com">J'introduirais ce terme plus tard </u> est un sujet qui intéresse de plus en
plus les designers et chercheureuses. Cela paraît évident puisque nous sommes tous·tes, de plus en plus,
impliqué·es dans ces problématiques environnementales puisque nous
sommes de plus en plus utilisateurices du numérique. Nous savons aujourd’hui
que le numérique a une incidence forte sur l’environnement, et même si cette pollution est plus difficile à visualiser que, par exemple, celle du plastique, l’impact est pourtant bien réel. Céline Lescop ingénieure et membre du Shift Project<span class="footnote">Le Shift Project est une association qui oeuvre pour une «&nbsp;économie libérée de la contrainte carbone&nbsp;», https://theshiftproject.org/ambition/</span>, précise que
3,5% de l’émission de <u>gaz à effet de serre</u> provient du numérique<span class="footnote">Ethics by Design - Quels enjeux politiques pour l’éco-conception de service numérique&nbsp;?, Conférence, 2020, (consulté le 10 juin 2021), [en ligne]</span>.
Elle précise que ces émissions de gaz à effet de serre ainsi que la
consommation d’électricité par le numérique ont, à ce jour, une
croissance de 9% par an. L’autre problème lié au numérique est
l’utilisation des ressources non-renouvelables&nbsp;: les métaux rares. Ces
extractions de métaux ont d’énormes impacts, comme la pollution de l’eau
ou encore la dégradation des écosystèmes. La pratique numérique, en
constante évolution, requiert de plus en plus d’équipements. Ainsi, elle provoque de plus en plus de besoins&nbsp;: le numérique est dans
une croissance systémique et il faudrait trouver les moyens pour
l’atténuer. Dans cette même conférence organisée par Designers Éthiques,
Adèle Chasson membre de l’association HOP<span class="footnote">HOP (Halte à l’Obsolescence Programmée) est une association qui milite pour l’allongement de la durée de vie des produits</span> précise que nous avons,
en moyenne, 99 appareils électriques/électroniques par foyer, et de plus
en plus connectés. Ce constat prouve à quel point il est important de
s’attarder sur la consommation en énergie de ce que nous produisons en
tant que designer. Philippe Vion-Dury, rédacteur en chef du magazine
*Socialter*, propose à cette table ronde la notion de «&nbsp;sobriété numérique
». Cette notion fait lien avec le discours de Victor J. Papanek&nbsp;: il s’agit
de créer du contenu plus sobre, sans superflu, et donc moins
consommateur. Comment faire mieux avec moins&nbsp;? Voilà une question qui se
pose aussi en design numérique.

Cette problématique est, entre autres, défendue par le studio Jeudimatin
installé dans l’habitation temporaire Maxima à Bruxelles. Les deux
membres de ce studio, Sven Grothe et Pascal Courtois, tous les deux
designers, souhaitent accompagner leurs projets «&nbsp;dans le design responsable<span class="footnote">https://jeudi.am</span>&nbsp;». Ils
proposent un web «&nbsp;accessible et inclusif&nbsp;» qui participerait à réduire
la consommation de ressources non-renouvelables. Nous pouvons lire sur
leur site leur charte dans laquelle il existe plusieurs points&nbsp;: d’abord
l’écologie, c’est-à-dire créer des sites alimentés en énergie
renouvelable pour réduire l’empreinte environnementale. Cet aspect rejoint les principes du site *Low-tech Magazine*¨<span class="footnote">https://solar.lowtechmagazine.com/fr/</span>.<u class="com">Tu ne cites pas Gauthier Roussillhe qui a mené une recherche importante sur le sujet</u>

Ce site a la particularité de fonctionner avec un serveur alimenté par énergie renouvelable
grâce à un panneau solaire et une batterie plomb-acide. Il se
retrouve donc parfois hors-ligne lorsque l’apport en énergie solaire
n’est plus suffisant, il dépend en fait de la météo à Barcelone, endroit
ou le serveur est localisé.

L’autre point cité par Jeudimatin est la transparence. Le site *Low-tech
Magazine* propose justement un indicateur de batterie visible sur le site
afin de prévenir les visiteureuses de la disponibilité du site. La couleur de l’arrière plan indique aussi le niveau de charge de la
batterie grâce à la hauteur de la couleur jaune. Ce site est alors à la
fois *low-tech* par la manière dont il est alimenté mais aussi à
travers sa conception et son apparence. La nouvelle simplification de leur site a permis de diviser par cinq la taille moyenne des pages du blog
par rapport à l’ancienne version&nbsp;: «&nbsp;Avec un site internet poids plume
alimenté par l’énergie solaire et déconnecté du réseau, nous voulons
démontrer que d’autres décisions peuvent être prises<span class="footnote">*Ibid.*</span>&nbsp;». Le poids
moyen d’une page est de 0,77 Mo, moins de la moitié du poids moyen d’une
page de blog et celui-ci est indiqué sur chaque page.

Le troisième point évoqué par Jeudimatin est la simplicité, autant dans
l’utilisation que dans la consommation de ressources énergétiques et
matérielles. Plusieurs paramètres ont permis au *Low-tech Magazine*
d’obtenir des pages si peu consommatrices. D’abord un site statique, qui
a pour avantage d’être «&nbsp;généré une fois pour toutes et existe comme un
simple ensemble de documents sur le disque dur du serveur&nbsp;», alors que
les sites dynamiques requièrent un nouveau calcul à chaque fois que le
site est ouvert. Ensuite, les images ont été optimisées pour réduire
leur poids. Ils ont utilisé une ancienne technique d’image appelée «&nbsp;diffusion d’erreur&nbsp;». La diffusion d’erreur est une technique qui permet
d’améliorer le rendu d’images numériques lorsque le nombre de couleurs
est diminué<span class="footnote">Diffusion d'erreur, Wikipédia (consulté le 20 octobre 2021), [en ligne]</span>. Les images sont alors toutes en noir et blanc, puis
colorées, ce qui permet de réduire leur poids tout en ayant un rendu de qualité
raisonnable. La police de caractère utilisée est celle par défaut pour
éviter une requête supplémentaire au serveur et de l’espace de stockage
en plus. Enfin, il n’y a «&nbsp;pas de pistage par un tiers, pas de services
de publicité, pas de cookies<span class="footnote">https://solar.lowtechmagazine.com/fr/</span>&nbsp;». Sans ces services, le trafic de
données est diminué et donc la consommation d’énergie l'est aussi. Grâce à la batterie, le site
peut rester en ligne pendant, au maximum, un ou deux jours de mauvais
temps. De ce fait, le *Low-tech Magazine* serait «&nbsp;hors ligne pendant une
moyenne de 35 jours par an&nbsp;».

Deux autres points sont abordés par Jeudimatin. L’accessibilité
et l’inclusion&nbsp;: des sites qui doivent être utilisables par le
plus grand nombre, en particulier par les personnes en situation de
handicap. Enfin, la notion d’utilité qui doit interroger sur les besoins et l'accès aux sites.

Il me semble justement que cette dernière notion d’utilité est une des
principales notions dans la transition vers l’éco-design. Le
fonctionnement du *Low-tech Magazine* permet une vraie remise en question
de nos pratiques avec cette notion d’utilité. Devons-nous,
lorsque ce n’est pas vraiment nécessaire, continuer à laisser en ligne nos sites
24h/24h&nbsp;? Ou encore, à quel moment devons-nous faire le choix entre un
site simple et peu consommateur et un site complexe&nbsp;? Peut-être plus
radicalement, quand devons-nous réaliser, ou non, un site&nbsp;? À ce propos Sophie Fétro écrit dans l'ouvrage collectif *Objectiver*<span class="footnote">*Objectiver*, ouvrage collectif dirigée par David-Olivier Lartigaud, 2017</span> «&nbsp;Apprendre à ne pas faire, ou apprendre à produire avec retenue, serait parfois nécessaire et gage d'une approche raisonnée du numérique&nbsp;», elle rajoute&nbsp;: «&nbsp;[...] peut-être pouvons-nous aussi saluer la dignité de l'être humain lorsqu'il ne fait pas, lorsqu'il s'abstient de fabriquer tout et n'importe quoi<span class="footnote">*Ibid.* Entretien avec Sophie Fétro, p. 149</span>&nbsp;».


## Conclusion


Ce mémoire a pour ambition de trouver des moyens radicaux pour créer en détournant, voire en refusant, les contraintes, les normes ou les inégalités, auxquelles nous sommes confronté·es dans notre société. Celles-ci peuvent être d’ordre politique, économique, sociale ou environnementale. Afin de trouver des solutions, je me suis appuyée sur le travail de plusieurs designers et sur les entretiens que j’ai pu faire avec elleux.D’abord c’est la notion de posture alternative que j’ai questionné. Après avoir récolté différentes définitions du terme «&nbsp;alternatif&nbsp;» propres à chaque designers que j’ai interrogé, puis après un rapide retour sur l’histoire des mouvements alternatifs, j’ai tenté de comprendre le but de tels choix. Nous l’avons vu, il existe de nombreuses manières d’être alternatif·ve : le lieu de travail, le fonctionnement au sein de son groupe, les outils ou les moyens de diffusions utilisés, les échanges non-monétaires, etc. Nous comprenons que l’adoption de postures alternatives permet d’échapper à certains systèmes dominants. Pour une rupture efficace avec l’ordre établi nous devrons être radicalement engagé·es dans ces choix alternatifs.J’ai ensuite choisi d’aborder la question de l’autonomie comme solutions pour échapper à ces contraintes. Être autonome nous permettrait de ne plus alimenter des systèmes que nous n’acceptons pas, c'est un acte politique engagé et radical. Mais nous l’avons compris, nous ne sommes jamais complètement autonome, il est alors important de choisir de qui nous voulons dépendre. C’est pourquoi je parle de co-dépendance choisie et c’est également la raison pour laquelle la notion de «&nbsp;communs&nbsp;» intervient. Faire du «&nbsp;commun&nbsp;» c’est accepter d’imaginer et de créer pour partager avec une communauté, c’est prôner l’accessibilité et l’inclusivité.Je me suis attardée sur cette notion d’autonomie envers nos outils de création et de diffusion en design graphique. C’est là que j’ai questionné les notions de détournement et de réappropriation. C’est alors le bricolage, le *hack* ou le piratage qui sont apparus comme des moyens pour acquérir l’autonomie face aux systèmes financiers et consuméristes de notre société. Plus précisemment c’est la création de nos outils, la mise en commun et la diffusion libre de ceux-ci ou de nos ressources en général, qui rendra possible une création plus libre et juste.Enfin c’est la question d’éco-conception, de *low-tech*, qui vient compléter ce mémoire. Puisque j’aborde l’importance de diffuser un projet, sous une forme tangible et sous une forme numérique, il était nécessaire de prendre en compte l’impact sur l’environnement que cela aura. Justement, ces projets hybrides, aussi appelés postnumériques par Florian Cramer, ne devront pas consommer plus d’énergie et de ressources qu’un projet diffusé sous une seule forme. C’est ici que je propose de penser la notion d’utilité comme solution vers une transition (éco)responsable du design graphique. Ne soyons pas des designers de tout et de rien. Tâchons de faire avec ce que nous avons, et sans ce qui ne nous semble pas juste ou utile.Les attentes envers les designers sont grandes dans ce projet, et peuvent paraître utopiques. C’est pour cela qu’il est important de faire devenir cette expérience, une expérience collective, de manière à ce que ce projet nous paraîsse moins irréalisable. De plus, il me semble que l’utopie est indispensable pour penser, imaginer et créer de nouveaux horizons. Évidement, aucun·e designer (sauf exception) ne peux mettre en œuvre toutes les notions abordées dans ce mémoire, néanmoins nous pouvons faire au mieux, changer nos habitudes pour refuser radicalement ce qui n’est plus acceptables aujourd’hui. Le temps a certainement toute son importance dans ce projet, il s’agit de voir à long terme pour circonscrire un tel projet. Loraine Furter, graphiste et chercheuse disait pendant sa conférence à la *Biennale Exemplaire* à Toulouse en 2021, que tout ce qui est fait dans l’urgence n’est pas bon. Alors certes, il y a urgence à devenir plus responsable, mais nous allons devoir être toujours plus actif·ve tout en acceptant le temps que cela prendra.

<br><br><br><br><br><br>
<b>«&nbsp;L'auteur nous dit que le designer serait comme un schizophrène. Il a raison. C'est aussi un équilibriste tentant d'impossibles figures où doivent s'unir des contraintes : d'un côté le marché, de l'autre l'être humain, le plastique et la nature, la fabrique et la légèreté, les pauvres et les
riches. C'est infini. Voilà où réside l'intérêt de ce métier, qui tente concrètement d'apporter des solutions dans ce flux d'ambiguïté. Quel designer suis-je&nbsp;? Quel designer voulez-vous être&nbsp;?<span class="footnote">*Cours traité du design*, Stéphane Vial, préface de Patrick Jouin, 2010</span>&nbsp;»  </b>

## Lexique

<u>Éthique</u>&nbsp;: Tirée du mot grec « ethos » qui signifie « manière de vivre », l'éthique est une branche de la philosophie qui s'intéresse aux comportements humains et, plus précisément, à la conduite des individus en société. L'éthique fait l'examen de la justification rationnelle de nos jugements moraux, elle étudie ce qui est moralement bien ou mal, juste ou injuste. https://www.canada.ca/fr/secretariat-conseil-tresor/services/valeurs-ethique/code/quest-ce-que-ethique.html#A1

<u>Autogestion</u>&nbsp;: Dans sa définition classique, l’autogestion (du grec autos, « soi-même », et « gestion ») est le fait, pour une structure ou un groupe d’individus considéré, de confier la prise des décisions le concernant à l’ensemble de ses membres. Elle suppose ainsi : la suppression de toute distinction entre dirigeants et dirigés, la transparence et la légitimité des décisions, la non-appropriation par certains des richesses produites par la collectivité et l'affirmation de l'aptitude des humains à s'organiser sans dirigeant.
 https://fr.wikipedia.org/wiki/Autogestion

<u>Économie collaborative</u>&nbsp;: l’économie collaborative ou économie de partage regroupe les activités économiques qui reposent sur le partage ou la mutualisation des biens, savoirs, services ou espaces et sur l'usage plutôt que la possession. https://fr.wikipedia.org/wiki/%C3%89conomie_collaborative

<u>Éclectisme</u>&nbsp;: Méthode intellectuelle consistant à emprunter à différents systèmes pour retenir ce qui paraît le plus vraisemblable et le plus positif dans chacun, et à fondre en un nouveau système cohérent les éléments ainsi empruntés https://www.cnrtl.fr/definition/%C3%A9clectisme

<u>Do It Yourself</u>&nbsp;: La philosophie se développe au début des années 70 avec l’émergence des mouvements hippies, puis punks. L’idée principale repose sur un rejet du principe de consommation massive : on fabrique soi-même pour ne pas avoir à acheter. https://www.nostrodomus.fr/do-it-yourself-petite-histoire-du-mouvement/

<u>Machine miméographique</u>&nbsp;: Appelé aussi duplicateur à pochoir ou miméographe, elle fonctionne en forçant l'encre à travers un pochoir à se déposer sur du papier où s'imprime par décalque le motif original. https://fr.wikipedia.org/wiki/Miméographe


<u>Fanzine</u>&nbsp;: Publication réalisée à moindres frais par des amateurs et passionnés d’un domaine particulier (science-fiction, musique, bande dessinée…), le fanzine est échangé à l’écart des circuits commerciaux traditionnels. Rencontrant un fort regain d’intérêt à l’occasion de l’engouement actuel pour le Do It Yourself dont il est un exemple graphique et culturel, le fanzine est ancré fortement dans les communautés artistiques. https://bbf.enssib.fr/consulter/bbf-2015-06-0038-005

<u>Réappropriation</u>&nbsp;: La réappropriation sert alors à désigner tout processus – lorsqu'il est lié à une critique d'un état d'aliénation, de dépossession, de perte – de reprise en main d'une activité, d'un savoir-faire et, plus largement, d'une reconquête de sa propre autonomie. https://fr.wikipedia.org/wiki/R%C3%A9appropriation

<u>Mouvement autonome</u>&nbsp;: Le mouvement autonome est un courant politique, culturel et social présent en Italie, en France, en Espagne et en Allemagne, et se réclamant de la lutte pour l’autonomie par rapport au capitalisme, à l’État, et aux syndicats https://fr.wikipedia.org/wiki/Mouvement_autonome

<u>Circuit court</u>&nbsp;: Un circuit court est un mode de commercialisation des produits agricoles qui s’exerce soit par la vente directe du producteur au consommateur, soit par la vente indirecte à condition qu’il n’y ait qu’un seul intermédiaire.  https://www.lelabo-ess.org/circuits-courts

<u>Subvention</u>&nbsp;: Une association déclarée peut recevoir des sommes d’argent appelées subventions, de la part de l’État, de collectivités territoriales et d’établissements publics. Ces sommes aident l’association à mener ses projets https://subventions.fr/dossier-subventions/les-subventions-aux-associations/

<u>Logiciel libre</u>&nbsp;: Logiciel dont l’utilisation, la modification, l’étude ou la duplication par autrui est autorisée, techniquement et légalement, dans le but de permettre aux utilisateurices d’avoir le contrôle sur le programme, d’avoir une certaine liberté, et de permettre le partage entre les individus. Il ne faut pas les confondre avec les logiciels gratuits propriétaires (freewares). https://fr.wikipedia.org/wiki/Logiciel_libre

<u>Pensée critique</u>&nbsp;: Concept dont les définitions sont nombreuses et parfois contradictoires, qui désigne, dans les grandes lignes, les capacités et attitudes permettant des raisonnements rigoureux afin d'atteindre un objectif, ou d'analyser des faits pour formuler un jugement.
https://fr.wikipedia.org/wiki/Esprit_critique

<u>Antidesign</u>&nbsp;: L'Antidesign est un mouvement apparu en Italie pendant les années 1960, en réaction à ce que de nombreux designers d'avant-garde considéraient comme le langage appauvri du modernisme, l'accent mis sur le style et l'esthétique de la bonne forme par de nombreux grands fabricants et designers renommés.
https://fr.wikipedia.org/wiki/Antidesign

<u>Risographe</u>&nbsp;: Un risographe, c’est un type de duplicopieur : une machine reposant sur la technique de reproduction par pochoir, permettant des impressions au rendu très graphique, en grandes quantités. La forme du risographe actuel émerge dans les années 80, mais le principe remonte au miméographe. http://maisonriso.fr/la-risographie/

<u>Pad</u>&nbsp;: Un « pad » est un éditeur de texte collaboratif en ligne. Les contributions de chaque utilisateur sont signalées par un code couleur, apparaissent à l’écran en temps réel et sont enregistrées au fur et à mesure qu’elles sont tapées. https://framapad.org/fr/

<u>Code source</u>&nbsp;: En informatique, le code source est un texte qui présente les instructions composant un programme sous une forme lisible, telles qu'elles ont été écrites dans un langage de programmation. Le code source se matérialise généralement sous la forme d'un ensemble de fichiers texte. Si les codes sources sont accessible et modifiable alors il s’agit d’un logiciel Open Source. https://fr.wikipedia.org/wiki/Code_source

<u>Hybride</u>&nbsp;: Cela correspondant à la formation d'un objet par l'action d'une multiplicité d'éléments qui, comme l'indique sa définition, crée, génère une nouvelle catégorie de formes, cette dimension dépassant largement celle de l'emprunt, de la combinatoire ou de la superposition pure et simple de techniques ou de pratiques. https://journals.openedition.org/leportique/851?lang=en

<u>Gaz à effet de serre</u>&nbsp;: Les gaz à effet de serre (GES) sont des composants gazeux qui absorbent le rayonnement infrarouge émis par la surface terrestre et contribuent ainsi à l’effet de serre. L'augmentation de leur concentration dans l’atmosphère terrestre est l'un des facteurs à l'origine du réchauffement climatique.
https://fr.wikipedia.org/wiki/Gaz_à_effet_de_serre

## Mise en page

Au vu de mon propos dans ce mémoire, je ne pouvais penser cette édition que comme un objet alternatif traduisant mon engagement. Plusieurs choix radicaux ont été faits pour répondre aux problématiques actuelles dans un souci d’éco-conception et d’accessibilité.

D’abord ce mémoire existe sous deux formes&nbsp;: une forme numérique, accessible en ligne, et une forme imprimée, en tant qu’objet tangible. Ces deux formes de publication ont la même source, c’est-à-dire un fichier HTML, qui peut être soit visible en ligne, soit imprimé&nbsp;: ce mémoire est une publication multisupport. La mise en page est adaptée au type de lecture, puisque le but d’une telle publication n’est pas de proposer une version compressée du fichier d’impression pour la lecture numérique. Dans la revue Back Office N°3 <span class="footnote">Back Office, Design graphique et pratiques numérique, Écrire l’écran, N°3, B42, 2019</span>, nous comprenons cette notion de différentes lectures en fonction des supports. Alexia de Visscher, designer graphique et enseignante écrit&nbsp;: «&nbsp;Tandis que les pratiques de lecture se modifient, la nécessité de proposer des contenus à la fois consultables à l’écran (en général plus rapidement, distant reading) et sur papier (lecture approfondie, close reading) se fait ressentir<span class="footnote">*Ibid.*, «&nbsp;Du design de la page à la pédagogie du flux&nbsp;: le cas belge&nbsp;», Alexia de Visscher,
p.122</span>&nbsp;». Alors, la mise en page du texte, le traitement des images, les sources ou encore la typographie sont, au mieux, adaptées au support de lecture. De plus, tout est fait pour rendre ce mémoire accessible&nbsp;: les notes de bas de page sont visibles et un lexique est proposé pour accompagner la lecture. Ce mémoire a donc été imprimé en 8 exemplaires et est disponible en version numérique, et imprimable, sur mon site personnel.

Dans sa forme, ce mémoire tient compte un maximum des problématiques environnementales. Tous les papiers utilisés sont des papiers récupérés autour de moi, souvent déjà imprimés sur une face. L’impression de ce mémoire a été réalisée au sein de l’école, en noir et blanc. Ce choix de support a largement influencé la forme de l’objet ainsi que la mise en page. D’abord la reliure est une reliure dos carré collé puisque les feuilles sont pliées en 2 afin de «&nbsp;dissimuler&nbsp;» la face du papier déjà utilisée. Ensuite les marges intérieures sont assez importantes afin de faciliter la lecture (cette reliure ne permet pas une ouverture complète de l’objet). Alors que les autres marges sont étroites, ce que permet ce façonnage, afin qu’un maximum de texte rentre sur une page et que le gaspillage d’espace et de papier soit évité. Enfin le format A6 permet de ne n'avoir aucune chute de papier. Les notes de bas de page sont présentes sur chaque page et non pas à la fin, toujours dans le but de faciliter la lecture. La même logique a été mise en place pour la version numérique&nbsp;: un code simple, des images légères, pour avoir un fichier *low-tech*.

Deux familles de caractères ont été utilisées. D’abord la *Meta-Old-French Links*, la *Meta-Old-French Poem* pour les citations et la *Meta-Old-French Pixel* pour le titre du mémoire et le foliotage. Ces deux styles sont des *forks* de la *Meta-Old-French regular* crée par Luuse en 2018, à partir du dessin de la *Hersey-Old-French*. Tout le travail d’évolution de ce caractère a été réalisé grâce à l’outil Plancton qui utilise le langage Metapost<span class="footnote">Metapost est un langage de programmation inspiré du langage Metafont crée par Donald Kuth. Il est développé la première fois en 1994 par John D.Hobby et permet de réaliser des graphiques vectorielles&nbsp;: lignes, courbes, transformations géométriques, etc. Wikipédia (consulté le 20 octobre 2021), [en ligne]</span>. Les deux premières versions utilisées dans ce mémoire ont été crée en 2021 avec l’aide des membres de Luuse et sont open source, la dernière a été réalisée sur Fontstruct, un outil gratuit de création de polices en ligne.<br>
La deuxième famille de caractère est le *Apfel Grotezk* de Luigi Gorlero, la version Regular est utilisée pour le texte courant. Pour les notes de bas de page, j’utilise la version *Brukt* afin de créer un contraste avec le texte courant. La particularité de la version *Brukt* est d'avoir été dessinée pour réduire la quantité d’encre utilisée pour l’impression grâce à des découpes de formes et de tailles variées à l’intérieur des lettres. Cette fonte est aussi *open source*.  

Enfin, les entretiens et l'iconographie ont été imprimés sur du papier de couleur, récupéré au sein de l'ESAD de Valence. Les images ont été traitées en niveau de gris. L'iconographie complète est imprimée sur une seule feuille A3 pour éviter la perte de papier. Chaque entretien est imprimé sur une feuille A4 ou A3 en fonction de la longueur de celui-ci. Ils ont ensuite été glissé dans le mémoire, à l'endroit où je les cite.

## Bibliographie

*Aesthetics of the commons*. Diaphanes, 2021.

**BARRY, Valentin**. *La portée des choses*. École Supérieure d'art et de design, 2011.

**BERNARD, Claude**. «&nbsp;Introduction à l’étude de la médecine expérimentale&nbsp;», Flammarion, 1865, p.65-66

**BLACK, Bob**. *Travailler, moi ? Jamais*. Les éditions du libre.

**BLAUVELT, Andrew**. « Outil (ou le designer graphique face à la post-production) ». *Azimuts*, n°47, 2017, p.88‑103.

**BLONDEAU, Olivier et LATRIVE, Florent**. *Libres enfants du savoir numérique*. L'éclat, 2000.

**BONJOUR, Marion**. *La liberté commence par l'autonomie*. École européenne supérieure d'art de Bretagne, 2017.

**BOUDEAU, Angélique**. *Des cycles et des hommes, le design graphique peut-il être écologique ?* 2016.

**BOURGUIGNON, Jonathan**. *Internet, année zéro: de la Silicon Valley à la Chine, naissance et mutations du réseau*. Editions Divergences, 2021.

*Écrire l'écran*, Back Office, «&nbsp;Design graphique et pratiques numériques n°3&nbsp;», 2019, B42

*Faire avec*, Back Office, «&nbsp;Design graphique et pratiques numériques n°1&nbsp;», 2017, B42

**CRAWFORD, Matthew B. et SAINT-UPÉRY, Marc**. *Éloge du carburateur: essai sur le sens et la valeur du travail*. La Découverte, 2010.

**DARRICAU, Stéphane**. *Culture graphique une perspective de Guttenberg à nos jours*. Pyramid, 2014.

«&nbsp;Design graphique & Culture visuelle&nbsp;», *Étapes* n° 222, 2014, p. 162

**KLEIN, Xavier**. *Libérons l'informatique !* 2013.

**LANDSBERG, Paul-Louis**. *Réflexions sur l'engagement personnel* Nouvelle éd, Editions Allia, 2021.

**LATRIVE, Florent**. *Du bon usage de la piraterie: culture libre, sciences ouvertes*. Nouvelle éd., la Découverte, 2007.

**LÉVI-STRAUSS, Claude**. *La penseé sauvage* Plon, 2014.

**LUDOVICO, Alessandro**. *La mutation de l'édition depuis 1894*. B42, 2016.

**MASURE, Anthony** *Design et humanités numériques* Éditions B42, 2017.

**MONDZAIN, Marie-José**. *Confiscation: des mots, des images et du temps*. 2017.

**OZERAY, Étienne**. *Pour un design graphique libre*. École nationale supérieure d'art et de design, 2014.

**PAPANEK, Victor J.**. *Design pour un monde réel*. Les presses du réel, 1971, traduit en 2021.

**PALLAYER, Pauline**. *Design graphique par temps d'orage*. École Supérieure d'art et de design, 2020.

**PEUGEOT, Valérie**. *Libres savoirs: les biens communs de la connaissance: produire collectivement, partager et diffuser les connaissances au XXIe siècle* C&F éditions, 2011.

**PHILIZOT, Vivien, et Jérôme Saint-Loubert Bié**. *Technique & design graphique: outils, médias, savoirs*. Éditions B42, Haute école des arts du Rhin, 2020.

**ROUSSILHE, Gaulthier**. *Vers l'autonomie du design*. 2017.

**TIBLOUX, Emmanuel et HUYGHES, Pierre-Damien**. «&nbsp;Design, mœurs et morale&nbsp;» *Azimuts*, n36, 2011, p. 71‑86.


## Webographie

<u>Dictionnaires / Encyclopédies&nbsp;:</u>

*Wikipédia, l’encyclopédie libre* [en ligne]
https://fr.wikipedia.org/wiki/Wikipé- dia:Accueil_principal

*Centre National de Ressources Textuelles et Lexicales* [en ligne] https://www.cnrtl.fr

*Dictionnaire Larousse* [en ligne] https://www.larousse.fr/dictionnaires/francais

<u>Conférence (audio)visuelles&nbsp;:</u>

**AYVAZOVA, Daria**. *Le designer défroqué*, [enregistrement vidéo], (consulté le 10 mai 2021), https://vimeo.com/210470404

**Designers Ethiques**. «&nbsp;Quels enjeux politiques pour l'éco-conception de service numérique&nbsp;?&nbsp;» [enregistrement vidéo], Conférence *Ethics by Design 2020*, (consulté le 20 septembre 2021) https://2020.ethicsbydesign.fr/replays/

**Designers Ethiques**. «&nbsp;Eusko, monnaie locale, exemple de design de politique publique&nbsp;» [enregistrement vidéo], Dante Edme-Sanjurjo - Conférence *Ethics by design*, (consulté le 10 septemble 2021) https://peertube.designersethiques.org/videos/watch/2def6514-75d1-45fb-a02f-49a352df3919

**Designers Ethiques**. «&nbsp;Les modèles alternatifs d'entreprises de design - Ethics by design 2020&nbsp;». [enregistrement vidéo] Conférence *Ethics by design*, (consulté le 20 septemble 2021) https://peertube.designersethiques.org/videos/watch/eb8290f0-3738-4cbd-92da-3bfcc6191d3f

«&nbsp;Marie-José Mondzain, Pour une radicalité vivante&nbsp;» [en ligne] *France Culture*, (consulté le 10 juin 2021) https://www.franceculture.fr/emissions/les-discussions-du-soir-avec-frederic-worms/marie-jose-mondzain-pour-une-radicalite

«&nbsp;Marie-José Mondzain, une philosophe "radicale"&nbsp;?&nbsp;» [en ligne] *France Culture*, (consulté le 10 juin 2021) https://www.franceculture.fr/emissions/la-grande-table-2eme-partie/marie-jose-mondzain-une-philosophe-radicale

**MONDZAIN, Marie-José**. *Pour une radicalité constructive* [en ligne], (consulté le 20 septembre 2021) https://www.youtube.com/watch?v=LIgyAMM5S04

<u>Périodiques / Articles / Blogs&nbsp;:</u>

«&nbsp;Architecture radicale&nbsp;». [en ligne] *Frac centre*, (consulté le 3 septembre 2021) https://www.frac-centre.fr/ressources/parcours-thematiques/architecture-radicale-127.html

**BANCE, Pierre**. «&nbsp;Antonio Negri et Michael Hardt, les mécanos de la Sociale&nbsp;». [en ligne] *Autre futur*, 2013, (consulté le 7 septembre 2021) http://www.autrefutur.net/IMG/pdf/Antonio_Negri_et_Michael_Hardt.pdf

**DEBRU, Claude**, «&nbsp;Claude Bernard : la médecine expérimentale&nbsp;» [en ligne], (Consulté le 4 octobre 2021) https://www.academie-sciences.fr/ archivage_site/activite/hds/textes/pse_Debru1.pdf

«&nbsp;De l'école d'ulm aux radicaux italiens&nbsp;» [en ligne] *master innovation by design*, (consulté le 20 septembre 2021) https://blog.ensci.com/innovationbydesign/2010/10/09/histoire-du-design-de-lecole-dulm-aux-radicaux-italiens

**DRABINSKI, Emily**. «&nbsp;Queering the Catalog: Queer Theory and the Politics of Correction&nbsp;» [en ligne] *The Library Quarterly*, vol. 83, n2, avril 2013, p. 94‑111, (consulté le 8 mai 2021) https://doi.org/10.1086/669547

«&nbsp;Ettore Sottsass et le design italien&nbsp;» [en ligne], (consulté le 20 septembre 2021) http://mediation.centrepompidou.fr/education/ressources/ENS-sottsass/ENS-sottsass.html

**FÉTRO, Sophie**. «&nbsp;Bricolages en design&nbsp;» [en ligne] *Techniques & Culture. Revue semestrielle d'anthropologie des techniques*, n64, décembre 2015, p. 152‑67. *journals.openedition.org*, (consulté le 18 mai 2021) https://doi.org/10.4000/tc.7577

**FURTER, Loraine**. «&nbsp;Trouble dans le genre - pédagogie alternative de l'édition hybride&nbsp;». [en ligne] *Design research x Hybrid publications*, 2018, (consulté le 18 mai 2021) https://www.design-research.be/hybrid/publications.html

**GOLSENNE, Thomas**. «&nbsp;Bricologie. La souris et le perroquet&nbsp;» [en ligne] *Techniques & Culture. Revue semestrielle d'anthropologie des techniques*, n64, décembre 2015, p. 128‑51. *journals.openedition.org*, (consulté le 10 octobre 2021) https://doi.org/10.4000/tc.7576

**HABER, Stéphane**. «&nbsp;La puissance du commun&nbsp;» [en ligne] *La Vie des idées*, mars 2010. *laviedesidees.fr*, (consulté le 18 octobre 2021) https://laviedesidees.fr/La-puissance-du-commun.html

«&nbsp;Josef Müller-Brockmann "swiss style"&nbsp;» [en ligne] *Graphéine - Agence de communication Paris Lyon*, 12 mars 2013, (consulté le 8 septembre 2021) https://www.grapheine.com/divers/graphic-designer-muller-brockmann

**MASURE, Anthony**. «&nbsp;Adobe : le créatif au pouvoir&nbsp;» [en ligne], (consulté le 24 mai 2021) https://www.anthonymasure.com/articles/2011-06-adobe-creatif-pouvoir

«&nbsp;Revue Multitudes&nbsp;» [en ligne] 2017/1 (n° 66). *Cairn.info*, (consulté le 2 juin 2021) https://www.cairn.info/revue-multitudes-2017-1.htm

«&nbsp;Ouvrir des chemins&nbsp;» [en ligne] *Cnap* (consulté le 9 juin 2021) https://www.cnap.fr/ouvrir-des-chemins

**PHILIZOT, Vivien**. «&nbsp;Graphisme et Transgression&nbsp;» [en ligne], (Consulté le 30 semptembre 2021) http://www.revue-signes.info/docu- ment.php?id=736

**PICHETTE, Jean**. «&nbsp;Médias, culture et radicalité : entretien avec Marie-José Mondzain&nbsp;» [en ligne] Liberté, 2017, p. 35-39, (consulté le 10 juillet 2021) https://www.erudit.org/fr/revues/liberte/2017-n318-liberte03419/87557ac/

**QUÉTEL, Claude**. «&nbsp;Histoire des murs&nbsp;» [en ligne] p. 295 à 306, 2012, (consulté le 18 octobre 2021) https://www.cairn.info/histoire-des-murs--9782262039547.htm  

## Remerciement

Quentin Juhel, Samuel Vermeil et Gilles Rouffineau pour leur précieux conseils tout au long de l'écriture de ce mémoire, ainsi que pour leur relecture. Je remercie également le reste de l'équipe pédagogique, Marie Gaspard, Annick Lantenois, Alexis Chazard, Dominique Cunin pour leur soutien.

Toutes les personnes avec qui j'ai pu m'entretenir pour ce mémoire et qui m'ont beaucoup apporté dans mon travail d'écriture&nbsp;: les membres de Luuse, Marion Bonjour, Sarah Garcin, Raphaël Bastide, les membres d'Objet-Papier et Les Rad!cales.

Et enfin, mes ami·es, ma famille ainsi que l'atelier Pré.faces pour leur soutien inconditionnel depuis le début.


## Colophon


Louise Sansoldi. Mémoire de fin d’étude réalisé dans le cadre du DNSEP Design Graphique à l’ESAD de Valence, 2021-2022.

Composé avec le caractère *Apfel Grotezk*, dessiné par Luigi Gorlero ainsi que la *Meta-Old-French Links* dessinée lors de mon stage chez Luuse et la *Meta-Old-French Pixel* dessinée sur Fontstruct. Imprimé sur des macules de papier, le plus souvent du papier machine, pour les pages intérieures. Papier ... pour la couverture. Achevé d’imprimer en 2021 à l'ESAD de Valence, France. Édité en 8 exemplaires.
